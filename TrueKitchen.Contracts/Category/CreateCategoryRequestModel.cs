﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.Category
{
    public class CreateCategoryRequestModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }
    }
}
