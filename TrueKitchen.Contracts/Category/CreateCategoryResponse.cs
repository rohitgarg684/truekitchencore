﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.Category
{
    public class CreateCategoryResponse
    {
        public CreateCategoryResponse(int categoryId)
        {
            CategoryId = categoryId;
        }

        [JsonProperty("category_id")]
        public int CategoryId { get; set; }
    }
}
