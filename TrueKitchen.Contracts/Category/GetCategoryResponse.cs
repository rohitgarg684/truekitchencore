﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.Category
{
    public class GetCategoryResponse
    {
        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }
    }
}
