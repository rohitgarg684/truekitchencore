﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class LoginUserRequest
    {
        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
