﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class SaveProfilePhotoResponse
    {
        public SaveProfilePhotoResponse(string url)
        {
            Url = url;
        }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
