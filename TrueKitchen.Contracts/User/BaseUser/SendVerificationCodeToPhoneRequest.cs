﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class SendVerificationCodeToPhoneRequest
    {
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        [JsonProperty("phone_no")]
        public string PhoneNo { get; set; }
    }
}
