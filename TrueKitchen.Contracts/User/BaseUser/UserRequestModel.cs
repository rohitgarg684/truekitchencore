﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public abstract class UserRequestModel
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("last_name")]
        public string LastNane { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone_no")]
        public string PrimaryPhoneNo { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
