﻿using Newtonsoft.Json;
using System.IO;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class FileUploadRequestModel
    {
        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [JsonProperty("file_size")]
        public int FileSize { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("stream")]
        public Stream Stream { get; set; }
    }
}