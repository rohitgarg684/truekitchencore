﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class AuthenticateUserResponse
    {
        public AuthenticateUserResponse(string token, int ttl)
        {
            Token = token;
            TTL = ttl;
            Success = true;
        }
        public AuthenticateUserResponse(AuthenticationError error)
        {
            Error = error;
            Success = false;
        }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("ttl")]
        public int TTL { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public AuthenticationError Error { get; set; }
    }

    public class AuthenticationError
    {
        public AuthenticationError(string message, int code)
        {
            Message = message;
            Code = code;
        }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }
    }
}
