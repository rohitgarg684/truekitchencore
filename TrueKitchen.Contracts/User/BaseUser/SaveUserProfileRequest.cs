﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class SaveUserProfileRequest
    {
        [JsonProperty("user_address")]
        public Address UserAddress { get; set; }

        [JsonProperty("personal_info")]
        public UserPersonalInfo PersonalInfo { get; set; }

        public class Address
        {
            [JsonProperty("line1")]
            public string AddressLine1 { get; set; }

            [JsonProperty("line2")]
            public string AddressLine2 { get; set; }

            [JsonProperty("zip_code")]
            public string ZipCode { get; set; }

            [JsonProperty("state")]
            public string State { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            [JsonProperty("lat")]
            public string Latitude { get; set; }

            [JsonProperty("long")]
            public string Longitude { get; set; }
        }

        public class UserPersonalInfo
        {
            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("primary_phone_no")]
            public string PrimaryPhoneNo { get; set; }

            [JsonProperty("secondry_phone_no")]
            public string SecondryPhoneNo { get; set; }
        }
    }
}
