﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class VerifyCodeToPhoneRequest
    {
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        [JsonProperty("phone_no")]
        public string PhoneNo { get; set; }

        [JsonProperty("otp")]
        public string VerificationCode { get; set; }
    }
}
