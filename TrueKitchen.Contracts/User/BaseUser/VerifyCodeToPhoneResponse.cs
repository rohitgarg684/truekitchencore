﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class VerifyCodeToPhoneResponse
    {
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        [JsonProperty("phone_no")]
        public string PhoneNo { get; set; }

        [JsonProperty("success")]
        public bool IsSuccess { get; set; }
    }
}
