﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class LoggedInUserDetailsResponse
    {
        [JsonProperty("user_id")]
        public Guid UserIdentifier { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("primary_phone_no")]
        public string PrimaryPhoneNo { get; set; }

        [JsonProperty("is_primary_phone_no_verified")]
        public bool IsPrimaryPhoneNoVerified { get; set; }

        [JsonProperty("secondry_phone_no")]
        public string SecondryPhoneNo { get; set; }

        [JsonProperty("is_secondry_phone_no_verified")]
        public bool IsSecondryPhoneNoVerified { get; set; }

        [JsonProperty("user_mailing_address")]
        public UserAddress UserMailingAddress { get; set; }

        public class UserAddress
        {
            [JsonProperty("line1")]
            public string AddressLine1 { get; set; }

            [JsonProperty("line2")]
            public string AddressLine2 { get; set; }

            [JsonProperty("zip_code")]
            public string ZipCode { get; set; }

            [JsonProperty("state")]
            public string State { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            [JsonProperty("lat")]
            public string Latitude { get; set; }

            [JsonProperty("long")]
            public string Longitude { get; set; }
        } 
    }
}
