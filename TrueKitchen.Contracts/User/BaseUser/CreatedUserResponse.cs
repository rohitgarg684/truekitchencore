﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.BaseUser
{
    public class CreatedUserResponse
    {
        public CreatedUserResponse(Guid userId)
        {
            UserId = userId;
        }

        [JsonProperty("user_id")]
        public Guid UserId { get; set; }
    }
}
