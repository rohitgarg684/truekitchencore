﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TrueKitchen.Contracts.RecipeModels;

namespace TrueKitchen.Contracts.User.Cook
{
    public class UpdateKitchenRequestModel
    {
        [JsonProperty("kitchen_name")]
        public string KitchenName { get; set; }

        [JsonProperty("desc")]
        public string KitchenDisscription { get; set; }

        [JsonProperty("is_kitchen_available")]
        public virtual bool IsKitchenAvailable { get; set; }

        [JsonProperty("kitchen_availabilities")]
        public virtual IList<KitchenAvailabilityModel> KitchenAvailabilities { get; set; }
    }

    public abstract class KitchenAvailabilityModel: AvailabilityModel
    {
    }
}
