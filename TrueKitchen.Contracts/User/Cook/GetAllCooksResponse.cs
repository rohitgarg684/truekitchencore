﻿using Newtonsoft.Json;
using System;

namespace TrueKitchen.Contracts.User.Cook
{
    public class GetCook
    {
        [JsonProperty("cook_id")]
        public Guid CookId { get; set; }
    }
    
    public class GetAllCooksResponse:Pagination.PagedDataResponse<GetCook>
    {
    }
}
