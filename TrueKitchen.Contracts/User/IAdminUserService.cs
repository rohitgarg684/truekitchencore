﻿using System.Threading.Tasks;
using TrueKitchen.Contracts.User.AdminUser;
using TrueKitchen.Contracts.User.BaseUser;

namespace TrueKitchen.Contracts.User
{
    public interface IAdminUserService
    {
        Task<CreatedUserResponse> CreateUser(CreateAdminUserRequestModel createAdminUserRequestModel);
    }
}
