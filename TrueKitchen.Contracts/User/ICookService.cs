﻿using System.Threading.Tasks;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Contracts.User.Cook;

namespace TrueKitchen.Contracts.User
{
    public interface ICookService
    {
        Task<CreatedUserResponse> CreateUser(CreateCookRequestModel createAdminUserRequestModel);
        Task<bool> UpdateKitchenDetails(UpdateKitchenRequestModel updateKitchenRequestModel);
        Task<GetAllCooksResponse> GetAllCooks(PageRequestModel pageRequestModel);
    }
}
