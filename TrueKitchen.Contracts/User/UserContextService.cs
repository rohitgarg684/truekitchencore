﻿using System.Threading.Tasks;
using TrueKitchen.Contracts.User.BaseUser;

namespace TrueKitchen.Contracts.User
{
    public interface IUserContextService
    {
        Task<LoggedInUserDetailsResponse> GetLoggedInUserDetails();
        Task<SaveProfilePhotoResponse> SaveProfilePhoto(FileUploadRequestModel fileUploadRequestModel);
        Task SaveUserProfile(SaveUserProfileRequest saveUserProfileRequest);
    }
}
