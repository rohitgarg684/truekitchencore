﻿using System.Threading.Tasks;
using TrueKitchen.Contracts.User.BaseUser;

namespace TrueKitchen.Contracts.User
{
    public interface IUserService
    {
        Task<bool> IsUserPresent(string phoneNo);
        Task<bool> IsUserNameAndPasswordValid(string userName, string password);
        Task<UserContext.UserContext> GetUserContextByPhoneNo(string phoneNo);
        Task<AuthenticateUserResponse> AuthenticateUser(LoginUserRequest loginUserRequest);
        Task<SendVerificationCodeToPhoneResponse> SendVerificationCodeToPhone(SendVerificationCodeToPhoneRequest sendVerificationCodeToPhoneRequest);
        Task<VerifyCodeToPhoneResponse> VerifyCodeToPhone(VerifyCodeToPhoneRequest verifyCodeToPhoneRequest);
        Task<bool> IsUserAuthenticated(string token);
    }
}
