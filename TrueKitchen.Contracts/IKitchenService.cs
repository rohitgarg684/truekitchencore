﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Contracts.Category;
using TrueKitchen.Contracts.RecipeModels;

namespace TrueKitchen.Contracts
{
    public interface IKitchenService
    {
        Task<RecipeViewModel> GetRecipeById(int recipeId);
        Task<RecipeCreatedResponse> AddRecipe(int categoryId, AddRecipeRequestModel model);
        Task<bool> AddTagtoRecipe(int recipeId, int tagId);
        Task<CreateCategoryResponse> CreateCategory(CreateCategoryRequestModel createCategoryRequestModel);
        Task<IList<RecipeViewModel>> GetRecipesByCook(Guid cookId);
        Task<GetRecipesByCategoryResponse> GetRecipesByCategory(int categoryId, Pagination.PageRequestModel pageRequestModel);
        Task<IList<GetCategoryResponse>> GetCategories();
        Task UpdateRecipe(SaveRecipeRequestModel model, int recipeId);
        Task DeleteRecipe(int recipeId);
    }
}
