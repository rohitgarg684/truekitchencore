﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TrueKitchen.Contracts.Pagination
{
    public class PagedDataResponse<T>
    {
        [JsonProperty("data")]
        public IEnumerable<T> Data { get;  set; }

        [JsonProperty("page_info")]
        public PageInfoModel PageInfo { get;  set; }
    }
}
