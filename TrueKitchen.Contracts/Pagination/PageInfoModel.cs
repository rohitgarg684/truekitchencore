﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.Pagination
{
    public class PageInfoModel
    {
        [JsonProperty("next_page_token")]
        public PageRequestModel NextPageToken {get;  set;}

        [JsonProperty("previous_page_token")]
        public PageRequestModel PreviousPageToken { get;   set; }

        [JsonProperty("total_count")]
        public int? TotalCount { get;  set; }
    }
}