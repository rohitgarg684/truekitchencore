﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.Pagination
{
    public class PageRequestModel
    {
        public PageRequestModel()
        {
            Asc = true;
            PageNo = 1;
        }

        [JsonProperty("pageNo")]
        public int PageNo { get; set; }

        [JsonProperty("pageSize")]
        public int PageSize { get; set; }

        [JsonProperty("asc")]
        public bool Asc { get; set; }
    }
}
