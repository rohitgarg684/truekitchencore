﻿namespace TrueKitchen.Contracts.UserContext
{
    public class UserContext
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsCook { get; set; }
        public bool IsCustomer { get; set; }
    }
}
