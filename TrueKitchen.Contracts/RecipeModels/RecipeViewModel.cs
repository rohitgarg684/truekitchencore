﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TrueKitchen.Contracts.RecipeModels
{
    public class RecipeViewModel: RecipeModel
    {
        [JsonProperty("recipe_id")]
        public int RecipeId { get; set; }
    }

    public class TagViewModel:TagModel
    {
       
    }

    public class IngredientViewModel: IngredientModel
    {
    }

    public class RecipeAvailabilityViewModel : RecipeAvailabilityModel
    {
       
    }
}
