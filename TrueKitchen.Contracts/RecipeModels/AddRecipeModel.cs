﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TrueKitchen.Contracts.RecipeModels
{
    public class AddRecipeRequestModel : RecipeRequestModel
    {
    }

    public abstract class RecipeModel
    {
        [JsonProperty("recipe_name")]
        public string RecipeName { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("is_veg")]
        public bool IsVeg { get; set; }

        [JsonProperty("is_vegan")]
        public bool? IsVegan { get; set; }

        [JsonProperty("serving_size")]
        public int? ServingSize { get; set; }

        [JsonProperty("contains_nuts")]
        public bool? IsContainingNuts { get; set; }

        [JsonProperty("contains_dairy")]
        public bool? IsContainingDairy { get; set; }

        [JsonProperty("tags")]
        public IList<TagRequestModel> Tags { get; set; }

        [JsonProperty("ingredients")]
        public IList<IngredientRequestModel> Ingredients { get; set; }

        [JsonProperty("is_recipe_available")]
        public virtual bool IsRecipeAvailable { get; set; }

        [JsonProperty("prep_time_min")]
        public virtual int PreparationTimeInMin { get; set; }

        [JsonProperty("recipe_availabilities")]
        public virtual IList<RecipeAvailabilityRequestModel> RecipeAvailabilities { get; set; }
    }
    public class RecipeRequestModel: RecipeModel
    {
        
    }

    public abstract class TagModel
    {
        [JsonProperty("name")]
        public string TagName { get; set; }
    }

    public class TagRequestModel:TagModel
    {
    }

    public abstract class IngredientModel
    {
        [JsonProperty("name")]
        public string IngredientName { get; set; }
    }

    public class IngredientRequestModel:IngredientModel
    {
    }

    public abstract class RecipeAvailabilityModel:AvailabilityModel
    {
       
    }
    
    public abstract class AvailabilityModel
    {
        [JsonProperty("start_time")]
        public TimeSpan StartTime { get; set; }

        [JsonProperty("end_time")]
        public TimeSpan EndTime { get; set; }

        [JsonProperty("day_of_the_week")]
        public string DayOfTheWeek { get; set; }
    }

    public class RecipeAvailabilityRequestModel:RecipeAvailabilityModel
    {
    }
}
