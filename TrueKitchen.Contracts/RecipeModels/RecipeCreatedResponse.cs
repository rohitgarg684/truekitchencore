﻿using Newtonsoft.Json;

namespace TrueKitchen.Contracts.RecipeModels
{
    public class RecipeCreatedResponse
    {
        [JsonProperty("recipe_id")]
        public int RecipeId { get; set; }
    }
}
