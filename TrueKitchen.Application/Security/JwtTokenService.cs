﻿using System.Threading.Tasks;
using TrueKitchen.Core.Security;

namespace TrueKitchen.Application.Security
{
    public class JwtTokenService : ITokenService
    {
        private readonly ITokenRepository _tokenRepository;

        public JwtTokenService(ITokenRepository tokenRepository)
        {
            _tokenRepository = tokenRepository;
        }

        public async Task<GenerateTokenServiceResponse> GenerateTokenAsync(string username)
        {
            var generateTokenResponse = await _tokenRepository.GenerateToken(username);
            return new GenerateTokenServiceResponse(generateTokenResponse.Token, generateTokenResponse.TTL);
        }

        public async Task<TokenValidationServiceResponse> ValidateToken(string token)
        {
            var validateTokenResponse = await _tokenRepository.ValidateToken(token);
            return new TokenValidationServiceResponse(validateTokenResponse.IsUserValidated, validateTokenResponse.UserName);
        }
    }
}
