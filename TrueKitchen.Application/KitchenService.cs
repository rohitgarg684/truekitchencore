﻿using System.Collections.Generic;
using AutoMapper;
using TrueKitchen.Contracts;
using System.Threading.Tasks;
using TrueKitchen.Contracts.Category;
using TrueKitchen.Contracts.RecipeModels;
using TrueKitchen.Contracts.UserContext;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Core.Repository.Interfaces;
using System;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Core.Models.Recipe;

namespace TrueKitchen.Application
{
    public class KitchenService : IKitchenService
    {
        private readonly IMapper _mapper;
        private readonly UserContext _userContext;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICookRepository _cookRepository;


        public KitchenService(IMapper mapper, UserContext userContext, ICategoryRepository categoryRepository, ICookRepository cookRepository)
        {
            _mapper = mapper;
            _userContext = userContext;
            _categoryRepository = categoryRepository;
            _cookRepository = cookRepository;
        }

        public async Task<RecipeCreatedResponse> AddRecipe(int categoryId, AddRecipeRequestModel addRecipeRequestModel)
        {
            var addRecipeModel = _mapper.Map<AddRecipeModel>(addRecipeRequestModel);
            addRecipeModel.CategoryId = categoryId;
            addRecipeModel.CookId = _userContext.UserId;
            var recipe = await _cookRepository.AddRecipe(addRecipeModel);
            return _mapper.Map<RecipeCreatedResponse>(recipe);
        }

        public async Task UpdateRecipe(SaveRecipeRequestModel saveRecipeRequestModel, int recipeId)
        {
            var updatedRecipeModel = _mapper.Map<UpdateRecipeModel>(saveRecipeRequestModel);
            updatedRecipeModel.CookId = _userContext.UserId;
            updatedRecipeModel.RecipeId = recipeId;
            await _cookRepository.UpdateRecipe(updatedRecipeModel);
        }

        public async Task DeleteRecipe(int recipeId)
        {
            await _cookRepository.DeleteRecipe(_userContext.UserId, recipeId);
        }

            public async Task<bool> AddTagtoRecipe(int recipeId, int tagId)
        {
            return true;
        }

        public async Task<CreateCategoryResponse> CreateCategory(CreateCategoryRequestModel createCategoryRequestModel)
        {
            var category = _mapper.Map<Category>(createCategoryRequestModel);
            return new CreateCategoryResponse(category.CategoryId);
        }

        public async Task<IList<GetCategoryResponse>> GetCategories()
        {
            var categorires= await _categoryRepository.Get(x=>true);
            return _mapper.Map<List<GetCategoryResponse>>(categorires);
        }

        public async Task<GetRecipesByCategoryResponse> GetRecipesByCategory(int categoryId, PageRequestModel pageRequestModel)
        {
            var pagerequest = _mapper.Map<Truekitchen.Infra.Domain.Pagination.PageRequest>(pageRequestModel);
            var recipesPagedResponse = await _cookRepository.GetRecipesByCategory(categoryId, pagerequest);
            return new GetRecipesByCategoryResponse
            {
                Data = _mapper.Map<IEnumerable<Recipe>, RecipeViewModel[]>(recipesPagedResponse.Data),
                PageInfo = _mapper.Map<PageInfoModel>(recipesPagedResponse.PageInfo)
            };
        }

        public async Task<IList<RecipeViewModel>> GetRecipesByCook(Guid cookId)
        {
            var recipes = await _cookRepository.GetRecipes(cookId);
            return _mapper.Map<List<RecipeViewModel>>(recipes);
        }

        public async Task<RecipeViewModel> GetRecipeById(int recipeId)
        {
            var recipe = await _cookRepository.GetRecipeById(recipeId);
            if (recipe == null)
                return null;
            return _mapper.Map<RecipeViewModel>(recipe);
        }
    }
}
