﻿using System.Threading.Tasks;
using AutoMapper;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Contracts.UserContext;
using TrueKitchen.Core.Data.Repository.User;
using TrueKitchen.Core.Security;
using TrueKitchen.Domain.Enum;
using TrueKitchen.Domain.ErrorCodes.User;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Application.User
{
    public  class UserService: IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly ITokenRepository _jwtTokenService;

        public UserService(IMapper mapper, IUserRepository userRepository, ITokenRepository jwtTokenService)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _jwtTokenService = jwtTokenService;
        }

        public async Task<bool> IsUserPresent(string phoneNo)
        {
            var user = await _userRepository.GetUserByPrimaryPhoneNo(phoneNo);
            return user!=null;
        }

        public async Task<bool> IsUserNameAndPasswordValid(string userName, string password)
        {
            var validationResponse=await _userRepository.IsPrimaryPhoneNoAndPasswordValid(userName, password);
            return !validationResponse.IsError;
        }

        public async Task<UserContext> GetUserContextByPhoneNo(string phoneNo)
        {
            var user = await _userRepository.GetUserByPrimaryPhoneNo(phoneNo);
            return new UserContext
            {
                Email = user.Email,
                UserId = user.UserId,
                IsAdmin = user.UserType==UserType.Admin,
                IsCook = user.UserType == UserType.Cook,
                IsCustomer = user.UserType==UserType.Customer
            };
        }

        public async Task<SendVerificationCodeToPhoneResponse> SendVerificationCodeToPhone(SendVerificationCodeToPhoneRequest sendVerificationCodeToPhoneRequest)
        {
            await _userRepository.SendVerificationCode(sendVerificationCodeToPhoneRequest.UserId, sendVerificationCodeToPhoneRequest.PhoneNo);
            return new SendVerificationCodeToPhoneResponse { PhoneNo = sendVerificationCodeToPhoneRequest.PhoneNo, UserId = sendVerificationCodeToPhoneRequest.UserId, IsSuccess = true };
        }


        public async Task<VerifyCodeToPhoneResponse> VerifyCodeToPhone(VerifyCodeToPhoneRequest verifyCodeToPhoneRequest)
        {
            await _userRepository.VerifyPhoneNoNotificationCode(verifyCodeToPhoneRequest.UserId, verifyCodeToPhoneRequest.PhoneNo, verifyCodeToPhoneRequest.VerificationCode);
            return new VerifyCodeToPhoneResponse { PhoneNo = verifyCodeToPhoneRequest.PhoneNo, UserId = verifyCodeToPhoneRequest.UserId, IsSuccess = true };
        }

        public async Task<AuthenticateUserResponse> AuthenticateUser(LoginUserRequest loginUserRequest)
        {
            var isUserNameCorrect = PhoneNo.Create(loginUserRequest.UserName);
            if (isUserNameCorrect.IsFailure)
            {
                return new AuthenticateUserResponse(new AuthenticationError(UserNameAndPasswordMismatch.ErrorMessage, UserNameAndPasswordMismatch.ErrorCode));
            }

            var userAuthenticationValidationResponse = await _userRepository.IsPrimaryPhoneNoAndPasswordValid(loginUserRequest.UserName, loginUserRequest.Password);
            if (userAuthenticationValidationResponse.IsError)
            {
                return new AuthenticateUserResponse(new AuthenticationError(UserNameAndPasswordMismatch.ErrorMessage, UserNameAndPasswordMismatch.ErrorCode));
            }

            var generateTokenResponse = await _jwtTokenService.GenerateToken(loginUserRequest.UserName);
            return new AuthenticateUserResponse(generateTokenResponse.Token, generateTokenResponse.TTL);
        }

        public async Task<bool> IsUserAuthenticated(string token)
        {
            var tokenValidationResponse = await _jwtTokenService.ValidateToken(token);

            if (!tokenValidationResponse.IsUserValidated || !await IsUserPresent(tokenValidationResponse.UserName))
                return false;

            return true;
        }
    }
}
