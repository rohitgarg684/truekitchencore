﻿using System.Threading.Tasks;
using AutoMapper;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.AdminUser;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Application.User
{
    public class AdminUserService: IAdminUserService
    {
        private readonly IAdminuserRepository _adminUserRepository;
        private readonly IMapper _mapper;

        public AdminUserService(IMapper mapper, IAdminuserRepository adminUserRepository)
        {
            _mapper = mapper;
            _adminUserRepository = adminUserRepository;
            
        }

        public async Task<CreatedUserResponse> CreateUser(CreateAdminUserRequestModel createAdminUserRequestModel)
        {
            var adminUser= _mapper.Map<AdminUser>(createAdminUserRequestModel);
            var createdUser= await _adminUserRepository.CreateUser(adminUser);
            return new CreatedUserResponse(createdUser.Identifier);
        }
    }
}
