﻿using AutoMapper;
using System.Threading.Tasks;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Contracts.UserContext;
using TrueKitchen.Core.Data.Repository.User;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Application.User
{
    public class UserContextService : IUserContextService
    {
        private readonly UserContext _userContext;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserContextService(IMapper mapper,  UserContext userContext, IUserRepository userRepository)
        {
            _mapper = mapper;
            _userContext = userContext;
            _userRepository = userRepository;
        }

        public async Task<LoggedInUserDetailsResponse> GetLoggedInUserDetails()
        {
            var user = await _userRepository.GetOne(x => x.UserId == _userContext.UserId);
            return _mapper.Map<LoggedInUserDetailsResponse>(user);

        }

        public async Task<SaveProfilePhotoResponse> SaveProfilePhoto(FileUploadRequestModel fileUploadRequestModel)
        {
            var fileUploadRequest = _mapper.Map<FileUploadRequest>(fileUploadRequestModel);
            var url = await _userRepository.SaveProfilePhoto(fileUploadRequest, _userContext.UserId);
            return new SaveProfilePhotoResponse(url);
        }

        public async Task SaveUserProfile(SaveUserProfileRequest saveUserProfileRequest)
        {
            var user = await _userRepository.GetOne(x => x.UserId == _userContext.UserId);
            var updatedProfileUser = _mapper.Map<SaveUserProfileRequest, Domain.Entities.User>(saveUserProfileRequest, user);
            await _userRepository.SaveProfile(updatedProfileUser);
            return ;
        }
    }
}
