﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Contracts.User.Cook;
using TrueKitchen.Contracts.UserContext;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Application.User
{
    public class CookService : ICookService
    {
        private readonly ICookRepository _cookRepository;
        private readonly IMapper _mapper;
        private readonly UserContext _userContext;
        private readonly ILogger _log;
        private readonly IUserService _userService;
        

        public CookService(IMapper mapper, ICookRepository cookRepository, UserContext userContext, ILogger log, IUserService userService)
        {
            _mapper = mapper;
            _cookRepository = cookRepository;
            _userContext = userContext;
            _log = log;
            _userService = userService;
        }

        public async Task<CreatedUserResponse> CreateUser(CreateCookRequestModel createCookRequestModel)
        {
            var cook = _mapper.Map<Cook>(createCookRequestModel);
            var createUser = await _cookRepository.CreateUser(cook);
            return new CreatedUserResponse(createUser.Identifier);
        }

        public async Task<bool> UpdateKitchenDetails(UpdateKitchenRequestModel updateKitchenRequestModel)
        {
            var cook = await _cookRepository.GetOne(x => x.UserId == _userContext.UserId);
            cook= _mapper.Map<UpdateKitchenRequestModel, Cook>(updateKitchenRequestModel, cook);
            await _cookRepository.Update(cook);
            await _cookRepository.Save();
            _log.LogInformation("Kitchen Updated");
            return true;
        }

        public async Task<GetAllCooksResponse> GetAllCooks(PageRequestModel pageRequestModel)
        {
            var pagerequest = _mapper.Map<Truekitchen.Infra.Domain.Pagination.PageRequest>(pageRequestModel);
            var cooksPagedResponse = await _cookRepository.GetCooks(pagerequest);
            return new GetAllCooksResponse
            {
                Data = _mapper.Map< IEnumerable<Cook>, GetCook[]>(cooksPagedResponse.Data),
                PageInfo = _mapper.Map<PageInfoModel>(cooksPagedResponse.PageInfo)
            };
        }
    }
}
