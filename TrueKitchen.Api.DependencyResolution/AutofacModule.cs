﻿using Autofac;
using System;
using TrueKitchen.Application;
using TrueKitchen.Application.Security;
using TrueKitchen.Application.User;

namespace TrueKitchen.Api.DependencyResolution
{
    public class AutofacModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<Data.AutofacModule>();

            builder
               .RegisterType<JwtTokenService>()
               .AsImplementedInterfaces()
               .SingleInstance();

            builder
              .RegisterType<UserContextService>()
              .AsImplementedInterfaces()
              .InstancePerLifetimeScope();

            builder
                .RegisterType<KitchenService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<AdminUserService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<CookService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<UserService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();


            //Registering domain services factory
            builder.Register<Func<string, Domain.Resolver.IResolver>>(container =>
            {
                var cc = container.Resolve<IComponentContext>();
                return (domainService) => cc.ResolveNamed<Domain.Resolver.IResolver>(domainService);
            });
        }

    }
}
