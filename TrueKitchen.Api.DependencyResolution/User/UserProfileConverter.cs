﻿using AutoMapper;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Api.DependencyResolution.User
{
    public class UserProfileConverter : ITypeConverter<SaveUserProfileRequest, Domain.Entities.User>
    {
        public Domain.Entities.User Convert(SaveUserProfileRequest source, Domain.Entities.User destination, ResolutionContext context)
        {
            if (source.PersonalInfo != null)
            {
                var personalInfo = source.PersonalInfo;
                destination.FirstName = personalInfo.FirstName;
                destination.LastName = personalInfo.LastName;
                destination.PrimaryPhoneNo = personalInfo.PrimaryPhoneNo;
                destination.SecondryPhoneNo = personalInfo.SecondryPhoneNo;
            }

            destination.Address = Resolve(source, destination, context);
            return destination;
        }

        private Address Resolve(SaveUserProfileRequest source, Domain.Entities.User destination, ResolutionContext context)
        {
            if (source.UserAddress == null)
                return destination.Address;

            var address = destination.Address ?? new Address();
            var addressModel = source.UserAddress;

            address.AddressLine1 = addressModel.AddressLine1;
            address.AddressLine2 = addressModel.AddressLine2;
            address.State = addressModel.State;
            address.ZipCode = addressModel.ZipCode;
            address.Country = addressModel.Country;
            address.Latitude = addressModel.Latitude;
            address.Longitude = addressModel.Longitude;
            return address;
        }
    }
}
