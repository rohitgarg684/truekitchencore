﻿using AutoMapper;
using TrueKitchen.Api.DependencyResolution.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Api.DependencyResolution
{
    public sealed class AutoMapperUserProfile : Profile
    {
        public AutoMapperUserProfile()
        {
            CreateMap<Contracts.User.BaseUser.UserRequestModel, Domain.Entities.User>()
                .Include<Contracts.User.AdminUser.CreateAdminUserRequestModel, Domain.Entities.AdminUser>()
                .Include<Contracts.User.Cook.CreateCookRequestModel, Domain.Entities.Cook>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastNane))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.UserPassword, opt => opt.MapFrom(src =>
                    new UserPassword(src.Password)));

            CreateMap<Contracts.User.AdminUser.CreateAdminUserRequestModel, Domain.Entities.AdminUser>();
            CreateMap<Contracts.User.Cook.CreateCookRequestModel, Domain.Entities.Cook>();


            CreateMap<Contracts.User.Cook.UpdateKitchenRequestModel, Domain.Entities.Cook>()
                .ForMember(dest => dest.KitchenDescription, opt => opt.MapFrom(src => src.KitchenDisscription))
                .ForMember(dest => dest.KitchenName, opt => opt.MapFrom(src => src.KitchenName));

            CreateMap<Contracts.User.BaseUser.FileUploadRequestModel, Domain.File.FileUploadRequest>();

            CreateMap<Domain.Entities.User, Contracts.User.BaseUser.LoggedInUserDetailsResponse>()
                .ForMember(dest => dest.UserIdentifier, opt => opt.MapFrom(src => src.Identifier))
                .ForMember(dest => dest.UserMailingAddress, opt =>
                        {
                            opt.Condition(src => src.AddressId > 0);
                            opt.MapFrom(src => new LoggedInUserDetailsResponse.UserAddress {
                                AddressLine1 = src.Address.AddressLine1,
                                AddressLine2 = src.Address.AddressLine2,
                                State = src.Address.State,
                                ZipCode = src.Address.ZipCode,
                                Country = src.Address.Country,
                                Latitude = src.Address.Latitude,
                                Longitude = src.Address.Longitude,
                            });
                        });

            CreateMap<Contracts.User.BaseUser.SaveUserProfileRequest, Domain.Entities.User>()
                 .ConvertUsing<UserProfileConverter>();

            CreateMap<Domain.Entities.Cook, Contracts.User.Cook.GetCook>()
                 .ForMember(dest => dest.CookId, opt => opt.MapFrom(src => src.Identifier));
        }
    }
}
