﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace TrueKitchen.Api.DependencyResolution
{
    public sealed class AutoMapperRecipeProfile : Profile
    {
        public AutoMapperRecipeProfile()
        {
           
            /************************SAVE RECIPE - FROM VIEW TO CORE************************************/
            CreateMap<Contracts.RecipeModels.TagRequestModel, Core.Models.Recipe.TagModel>();
            CreateMap<Contracts.RecipeModels.IngredientRequestModel, Core.Models.Recipe.IngredientModel>();
            CreateMap<Contracts.RecipeModels.RecipeAvailabilityRequestModel, Core.Models.Recipe.RecipeAvailabilityModel>();
            CreateMap<Contracts.RecipeModels.SaveRecipeRequestModel, Core.Models.Recipe.UpdateRecipeModel>();
            /************************************************************/

            /***************************ADD RECIPE - FROM VIEW TO CORE ***********************/
            CreateMap<Contracts.RecipeModels.AddRecipeRequestModel, Core.Models.Recipe.AddRecipeModel>();
            /*****************************************************/


            /**********************SAVE RECIPE - FROM CORE TO DOMAIN**************************/
            CreateMap<Core.Models.Recipe.UpdateRecipeModel, Domain.Entities.Recipe>()
                .ForMember(dest => dest.RecipeId, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeIngredients, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeTags, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeAvailabilities, opt => opt.Ignore());
            /************************************************/

            /**********************ADD RECIPE - FROM CORE TO DOMAIN**************************/
            CreateMap<Core.Models.Recipe.RecipeAvailabilityModel, Domain.Entities.RecipeAvailability>();

            CreateMap<Core.Models.Recipe.AddRecipeModel, Domain.Entities.Recipe>()
                .ForMember(dest => dest.RecipeId, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeIngredients, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeTags, opt => opt.Ignore())
                .ForMember(dest => dest.RecipeAvailabilities, opt => opt.Ignore());
            /************************************************/

            /**********************ADD RECIPE - FROM DOMAIN TO VIEW**************************/
            CreateMap<Domain.Entities.Recipe, Contracts.RecipeModels.RecipeCreatedResponse>()
                .ForMember(dest => dest.RecipeId, opt => opt.MapFrom(src => src.RecipeId));
            /************************************************/


            /**********************VIEW RECIPE - FROM DOMAIN TO VIEW**************************/
            CreateMap< Domain.Entities.RecipeTag, Contracts.RecipeModels.TagViewModel>()
                .ForMember(dest => dest.TagName, opt => opt.MapFrom(src => src.TagName));

            CreateMap<Domain.Entities.RecipeIngredient, Contracts.RecipeModels.IngredientViewModel>()
               .ForMember(dest => dest.IngredientName, opt => opt.MapFrom(src => src.Name));

            CreateMap<Domain.Entities.RecipeAvailability, Contracts.RecipeModels.RecipeAvailabilityViewModel>()
                .ForMember(dest => dest.DayOfTheWeek, opt => opt.MapFrom(src => src.DayOfTheWeek.ToString()));


            CreateMap<Domain.Entities.Recipe, Contracts.RecipeModels.RecipeViewModel>()
                .ForMember(dest => dest.Ingredients, opt => opt.ResolveUsing((src, dest, destMember, context) => context.Mapper.Map<IList<Contracts.RecipeModels.IngredientViewModel>>(src.RecipeIngredients)))
                .ForMember(dest => dest.Tags, opt => opt.ResolveUsing((src, dest, destMember, context) => context.Mapper.Map<IList<Contracts.RecipeModels.TagViewModel>>(src.RecipeTags)));
            /************************************************/
        }

    }
}
