﻿using AutoMapper;
using TrueKitchen.Contracts.Pagination;

namespace TrueKitchen.Api.DependencyResolution.Pagination
{
    public class PaginationRequestModelTypeConverter : ITypeConverter<Truekitchen.Infra.Domain.Pagination.PageRequest, PageRequestModel>
    {
        public PageRequestModel Convert(Truekitchen.Infra.Domain.Pagination.PageRequest source, PageRequestModel destination, ResolutionContext context)
        {
            return new PageRequestModel
            {
                Asc = source.Asc,
                PageNo = source.PageNo,
                PageSize = source.PageSize
            };
        }
    }
}
