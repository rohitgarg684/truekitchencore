﻿using AutoMapper;
using System.Linq;
using Truekitchen.Infra.Domain.Pagination;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Core.Exceptions;

namespace TrueKitchen.Api.DependencyResolution.Pagination
{
    public class PageRequestTypeConverter : ITypeConverter<PageRequestModel, PageRequest>
    {
        public PageRequest Convert(PageRequestModel source, PageRequest destination, ResolutionContext context)
        {
            var pageNoValidationResponse = PageRequest.ValidatePageSize(source.PageSize);

            if (pageNoValidationResponse.IsError)
                throw new PaginationException(pageNoValidationResponse.ErrorResponses.First().ErrorMessage);

            return new PageRequest
            {
                Asc = source.Asc,
                PageNo = source.PageNo,
                PageSize = source.PageSize
            };
        }
    }
}
