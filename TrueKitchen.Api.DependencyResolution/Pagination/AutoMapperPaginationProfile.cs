﻿using AutoMapper;
using TrueKitchen.Contracts.Pagination;

namespace TrueKitchen.Api.DependencyResolution.Pagination
{
    public class AutoMapperPaginationProfile : Profile
    {
        public AutoMapperPaginationProfile()
        {
            CreateMap<PageRequestModel, Truekitchen.Infra.Domain.Pagination.PageRequest>()
                 .ConvertUsing<PageRequestTypeConverter>();

            CreateMap<Truekitchen.Infra.Domain.Pagination.PageRequest, PageRequestModel>()
                .ConvertUsing<PaginationRequestModelTypeConverter>();

            CreateMap<Truekitchen.Infra.Domain.Pagination.PageInfo, PageInfoModel>()
                 .ForMember(dest => dest.TotalCount, opt => opt.MapFrom(src => src.TotalCount))
                  .ForMember(dest => dest.PreviousPageToken, opt => opt.ResolveUsing((src, dest, destMember, context) => context.Mapper.Map<PageRequestModel>(src.PreviousPageToken)))
                  .ForMember(dest => dest.NextPageToken, opt => opt.ResolveUsing((src, dest, destMember, context) => context.Mapper.Map<PageRequestModel>(src.NextPageToken)));
        }
    }
}
