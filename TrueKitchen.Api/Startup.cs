﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using TrueKitchen.Api.Middleware.Cors;
using TrueKitchen.Api.Middleware;
using TrueKitchen.Api.Swagger;
using System;
using TrueKitchen.Api.Middleware.Autofac;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TrueKitchen.Infra.Logging;
using TrueKitchen.Api.Middleware.Authorization;
using Microsoft.AspNetCore.Http;

namespace TrueKitchen.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        //this method gets called at runtime
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //setup pathBase for allowing subdirectories to host application and no broken links
            var appPrefix = "true-kitchen-api";
            app.UsePathBase("/"+ appPrefix);

            app.UseApiInfaMiddlewares();
            //add cors
            app.AddCors();

            //add authentication
            app.UseAuthentication();

            //add http routes info
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => {

                c.SwaggerEndpoint("/swagger/v1/swagger.json", appPrefix);
            });
        }

        //this method gets called at runtime.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //services should come first
            services.AddAuthorizationForApi();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    var settings = options.SerializerSettings;
                    settings.Converters.Add(new StringEnumConverter());
                    settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            services.AddSwagger();


            var fileConfig = new ConfigurationBuilder()
              .SetBasePath(Environment.ContentRootPath)
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .Build();

            var mapper = AutoMapperConfig.Register();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            AutofacConfig.Configure(containerBuilder, mapper, fileConfig);

           var container = containerBuilder.Build();
           var serviceProvider = new AutofacServiceProvider(container);

            var logFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            NlogConfiguration.RegisterLogger(logFactory);

            return serviceProvider;
        }
    }
}
