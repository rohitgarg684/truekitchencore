﻿using AutoMapper;
using TrueKitchen.Api.DependencyResolution;
using TrueKitchen.Api.DependencyResolution.Pagination;

namespace TrueKitchen.Api.Middleware.Autofac
{
    public static class AutoMapperConfig
    {
        /// <summary>
        ///     Registers app settings from configuration providers.
        /// </summary>
        /// <returns></returns>
        public static IMapper Register()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
                cfg.AddProfile<AutoMapperUserProfile>();
                cfg.AddProfile<AutoMapperPaginationProfile>();
                cfg.AddProfile<AutoMapperRecipeProfile>();
            });
            return config.CreateMapper();
        }
    }
}