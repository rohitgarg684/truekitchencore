﻿using Autofac;
using AutoMapper;
using TrueKitchen.Api.DependencyResolution;
using TrueKitchen.Contracts.UserContext;
using System.Net.Http;
using System.Threading.Tasks;
using TrueKitchen.Contracts.User;
using Microsoft.Extensions.Configuration;
using TrueKitchen.Core;
using TrueKitchen.Core.Parameters;
using TrueKitchen.Core.Security;
using TrueKitchen.Infra.Api.AutofacExtensions;
using TrueKitchen.Api.Services;
using TrueKitchen.Infra.Logging;
using Microsoft.AspNetCore.Http;
using TrueKitchen.Infra.Api.Middleware.Authorization;

namespace TrueKitchen.Api.Middleware.Autofac
{
    /// <summary>
    /// Represent Autofac configuration.
    /// </summary>
    public static class AutofacConfig
    {
        /// <summary>
        /// Configured instance of <see cref="IContainer"/>
        /// <remarks><see cref="AutofacConfig.Configure"/> must be called before trying to get Container instance.</remarks>
        /// </summary>
        public static IContainer Container;

        /// <summary>
        /// Initializes and configures instance of <see cref="IContainer"/>.
        /// </summary>
        /// <param name="configuration"></param>
        public static void Configure(ContainerBuilder builder, IMapper typeMapper, IConfiguration fileConfig)
        {
            builder.AddLoggingServices("truekitchen-app");

            builder.Register(cfg => typeMapper)
                .As<IMapper>()
                .SingleInstance();

            //RegisterDB Options
            RegisterAppsettings(builder, fileConfig);

            // Other components can be registered here.
            builder.RegisterModule(new AutofacModule());


            builder
             .RegisterType<AuthenticationService>()
             .AsImplementedInterfaces();

            builder.Register<UserContext>(c =>
            {
                var context = c.ResolveOptional<IHttpContextAccessor>();
                var request = context.HttpContext.Request;
                if (request != null)
                {
                    try
                    {
                        var token = AuthUtil.GetAuthTokenFromRequest(request);
                        if (!string.IsNullOrEmpty(token))
                        {
                            var tokenService = c.Resolve<ITokenService>();
                            var userService = c.Resolve<IUserService>();
                            var tokenValidationResponse = Task.Run(() => tokenService.ValidateToken(token)).Result;
                            var userContext = Task.Run(() => userService.GetUserContextByPhoneNo(tokenValidationResponse.UserName)).Result;
                            return userContext;
                        }
                    }
                    catch
                    {
                        return new UserContext();
                    }
                }

                return new UserContext();
            }).AsSelf().InstancePerLifetimeScope();

        }

        private static void RegisterAppsettings(ContainerBuilder builder, IConfiguration fileConfig)
        {
            builder.RegisterAppsettings<ConnectionParameters>(fileConfig);
            builder.RegisterAppsettings<JwtParameters>(fileConfig);
            builder.RegisterAppsettings<ImageFileParameters>(fileConfig);
            builder.RegisterAppsettings<IpStackParameters>(fileConfig);
            builder.RegisterAppsettings<MobileNoVerificationParameters>(fileConfig);
        }
    }
}