﻿namespace TrueKitchen.Api.Middleware.Authorization
{
    public class UserPolicyTypes
    {
        public const string AdminLoggedIn = "AdminLoggedIn";
        public const string CookLoggedIn = "CookLoggedIn";
        public const string CustomerLoggedIn = "CustomerLoggedIn";
    }
}
