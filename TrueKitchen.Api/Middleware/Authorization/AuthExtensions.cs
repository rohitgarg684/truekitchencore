﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using TrueKitchen.Infra.Api.Middleware.Authorization;

namespace TrueKitchen.Api.Middleware.Authorization
{
    public static class AuthExtensions
    {
        public static void AddAuthorizationForApi(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(UserPolicyTypes.AdminLoggedIn);
                options.AddPolicy(UserPolicyTypes.CookLoggedIn);
                options.AddPolicy(UserPolicyTypes.CustomerLoggedIn);
            });

            services.AddTrueKichenAuthorization(new List<string> { UserPolicyTypes.AdminLoggedIn, UserPolicyTypes.CookLoggedIn, UserPolicyTypes.CustomerLoggedIn });
        }

        private static void AddPolicy(this AuthorizationOptions options, string policyName)
        {
            options.AddPolicy(policyName,
             policy =>
            {
                policy.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme);
                policy.Requirements.Add(new LoggedInUserRequirement(new List<string> { policyName }));
            });
        }
    }
}
