﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Api.Middleware.Authorization;
using TrueKitchen.Contracts.User;
using TrueKitchen.Core.Security;
using TrueKitchen.Infra.Api.Filters.Interfaces;

namespace TrueKitchen.Api.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;

        public AuthenticationService(IUserService userService, ITokenService tokenService)
        {
            _userService = userService;
            _tokenService = tokenService;
        }

        public async Task<bool> IsUserAuthorizedWithAnyOfTypes(string token, IList<string> userTypes)
        {
            var tokenValidationResponse = await _tokenService.ValidateToken(token);
            if (!tokenValidationResponse.IsUserValidated)
                return false;

            var userContext = await _userService.GetUserContextByPhoneNo(tokenValidationResponse.UserName);

            bool IsAdmin = userContext.IsAdmin && userTypes.Contains(UserPolicyTypes.AdminLoggedIn);
            bool IsCook = userContext.IsCook && userTypes.Contains(UserPolicyTypes.CookLoggedIn);
            bool isCustomer = userContext.IsCustomer && userTypes.Contains(UserPolicyTypes.CustomerLoggedIn);

            if (IsAdmin || IsCook || isCustomer)
                return true;

            return false;
        }
    }
}