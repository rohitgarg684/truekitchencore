﻿using Microsoft.AspNetCore.Builder;

namespace TrueKitchen.Api.Middleware.Cors
{
    public static class Cors
    {
        public static void AddCors(this IApplicationBuilder app)
        {
            app.UseCors(builder => builder
            .WithOrigins("*")
            .AllowAnyMethod()
            .AllowAnyHeader());
        }
    }
}
