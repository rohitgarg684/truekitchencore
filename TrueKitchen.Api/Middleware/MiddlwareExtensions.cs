﻿using Microsoft.AspNetCore.Builder;
using TrueKitchen.Infra.Api.Middleware.Exception;
using TrueKitchen.Infra.Api.Middleware.Headers;
using TrueKitchen.Infra.Api.Middleware.Metrics;
using TrueKitchen.Infra.Api.Middleware.HealthCheck;

namespace TrueKitchen.Api.Middleware
{
    public static class MiddlwareExtensions
    {
        public static void UseApiInfaMiddlewares(this IApplicationBuilder app)
        {
            //order is important
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<ErrorLoggingMiddleware>();
            app.UseMiddleware<HealthCheckMiddleware>();
            app.UseMiddleware<TrackingRequestResponseMiddleware>();
            app.UseMiddleware<PerformanceMetricsMiddleware>();
        }
    }
}
