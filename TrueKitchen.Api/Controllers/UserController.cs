﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Infra.Api.Middleware.Authorization;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Controllers
{
    [Route("user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserContextService _userContextService;

        public UserController(IUserService userService, IUserContextService userContextService)
        {
            _userService = userService;
            _userContextService = userContextService;
        }

        /// <summary>
        /// This call is used to check if the user is already present in the system. Entering the email address we can identify whether a user is present or not. Please note:  A user can either be cook, admin or normal user (who requests food).
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns></returns>
        [HttpPost]
        [Route("available")]
        [ProducesResponseType(200,Type=typeof(bool))]
        public async Task<IActionResult> IsUserPresent([FromBody] string primaryPhoneNo)
        {
            return Ok(await _userService.IsUserPresent(primaryPhoneNo));
        }

        /// <summary>
        /// Login into the system by getting a token, for username enter your primary phone no, for india users use +91 and for US use +1
        /// </summary>
        /// <param name="loginUserRequest">token</param>
        /// <returns>token value along with ttl if logged in successfully else retuens success flag as false.</returns>
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(200, Type = typeof(AuthenticateUserResponse))]
        public async Task<IActionResult> Login([FromBody] LoginUserRequest loginUserRequest)
        {
            var authenticateUserResponse = await _userService.AuthenticateUser(loginUserRequest);
            return Ok(authenticateUserResponse);
        }


        /// <summary>
        /// Add profile photo for the user
        /// </summary>
        /// <param name="request">request paramter for uploading file</param>
        /// <returns></returns>
        [HttpPost]
        [Route("profilePhoto")]
        [Authorize(AuthPolicy.UserLoggedIn)]
        [ProducesResponseType(200, Type = typeof(SaveProfilePhotoResponse))]
        public async Task<IActionResult> AddProfilePhoto(List<IFormFile> files)
        {
            foreach (var file in files)
            {
                var imageUploadRequestModel = new FileUploadRequestModel
                {
                    ContentType = file.ContentType.ToString(),
                    FileSize = (int) file.Length,
                    FileName = file.FileName.Trim('\"')
                };

                await file.CopyToAsync(imageUploadRequestModel.Stream);
                var response = await _userContextService.SaveProfilePhoto(imageUploadRequestModel);
                return Ok(response);
            }

            return Ok();
        }

        /// <summary>
        /// this call would give profile information of the logged in user. 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("me")]
        [Authorize(AuthPolicy.UserLoggedIn)]
        [ProducesResponseType(200, Type = typeof(AuthenticateUserResponse))]
        public async Task<IActionResult> GetLoggedInUserDetails()
        {
            return Ok(await _userContextService.GetLoggedInUserDetails());
        }


        /// <summary>
        /// this call would let you update profile informatio of the user like name, phone no, address etc
        /// </summary>
        /// <param name="saveUserProfileRequest"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("profile")]
        [Authorize(AuthPolicy.UserLoggedIn)]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> SaveUserProfile([FromBody] SaveUserProfileRequest saveUserProfileRequest)
        {
            await _userContextService.SaveUserProfile(saveUserProfileRequest);
            return Ok();
        }


        /// <summary>
        /// this would send you the OTP code for the mobile number. This call will be used during signup so no authenitcation is there. 
        /// </summary>
        /// <param name="sendVerificationCodeToPhoneRequest">Pass userid  and phone no</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendVerificationCode")]
        [ProducesResponseType(200, Type = typeof(SendVerificationCodeToPhoneResponse))]
        public async Task<IActionResult> SendVerificationCodeToPhone([FromBody] SendVerificationCodeToPhoneRequest sendVerificationCodeToPhoneRequest)
        {
            return Ok(await _userService.SendVerificationCodeToPhone(sendVerificationCodeToPhoneRequest));
        }


        /// <summary>
        ///  This call is used to verify the otp code.
        /// </summary>
        /// <param name="verifyCodeToPhoneRequest">Pass userid, otp and phone no</param>
        /// <returns></returns>
        [HttpPost]
        [Route("verifyMobileCode")]
        [ProducesResponseType(200, Type = typeof(VerifyCodeToPhoneResponse))]
        public async Task<IActionResult> VerifyCodeToPhone([FromBody] VerifyCodeToPhoneRequest verifyCodeToPhoneRequest)
        {
            return Ok(await _userService.VerifyCodeToPhone(verifyCodeToPhoneRequest));
        }
    }
}