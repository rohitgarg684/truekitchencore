﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TrueKitchen.Api.Middleware.Authorization;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.AdminUser;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Infra.Api.Middleware.Authorization;

namespace TrueKitchen.Controllers
{
    [Route("admin")]
    public class AdminUserController : Controller
    {
        private readonly IAdminUserService _adminUserService;
        
        public AdminUserController(IAdminUserService adminUserService)
        {
            _adminUserService = adminUserService;
        }

        /// <summary>
        /// CreateUser will create an admin user for the system which have advanced priveleges and access rights. Only an Admin user can create another admin user.
        /// </summary>
        /// <param name="createAdminUserRequestModel"> First name, last name, email and password are mandatory fields</param>
        /// <returns>if you get  Guid means admin user got successfully created. You can login into the system with your email and password used for creating the user</returns>
        [HttpPut]
        [Route("user")]
        [Authorize(UserPolicyTypes.AdminLoggedIn)]
        [ProducesResponseType(200, Type = typeof(CreatedUserResponse))]
        public async Task<IActionResult> CreateUser([FromBody] CreateAdminUserRequestModel createAdminUserRequestModel)
        {
            return Ok(await _adminUserService.CreateUser(createAdminUserRequestModel));
        }
    }
}