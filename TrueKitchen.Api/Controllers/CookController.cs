﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TrueKitchen.Api.Middleware.Authorization;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Contracts.User;
using TrueKitchen.Contracts.User.BaseUser;
using TrueKitchen.Contracts.User.Cook;
using TrueKitchen.Infra.Api.Middleware.Authorization;

namespace TrueKitchen.Controllers
{
    [Route("cook")]
    public class CookController : Controller
    {
        private readonly ICookService _cookService;

        public CookController(ICookService cookService)
        {
            _cookService = cookService;
        }

        /// <summary>
        /// CreateUser will create a cook user for the system which have priveleges and access rights for the role of cook.  You can add dishes or do a lot more by logging into the system with email and password used to create the user.
        /// </summary>
        /// <param name="createAdminUserRequestModel"> First name, last name, email and password are mandatory fields</param>
        /// <returns>if sucessfuly return user_id for the cook. You can login into the system with your email and password used for creating the user</returns>
        [HttpPut]
        [Route("user")]
        [ProducesResponseType(200, Type = typeof(CreatedUserResponse))]
        public async Task<IActionResult> CreateUser([FromBody] CreateCookRequestModel createCookRequestModel)
        {
            var createdUser = await _cookService.CreateUser(createCookRequestModel);
            return Ok(createdUser);
        }

        /// <summary>
        /// Once logged in by cook (with their email and password). Cook can update its Kitchen details which are name and description. Each cook has one kitchen and vice versa. 
        /// </summary>
        /// <param name="updateKitchenRequestModel">Kitchen name and description is required. Any user can read the description and kitchen name to get an idea what kind of food this kitchen serves. Eg: Punjabi Vaishno Rasoi is a good kitchen name</param>
        /// <returns></returns>
        [Authorize(UserPolicyTypes.CookLoggedIn)]
        [HttpPost]
        [Route("kitchen")]
        [ProducesResponseType(200, Type = typeof(bool))]
        public async Task<IActionResult> UpdateKitchenDetails([FromBody] UpdateKitchenRequestModel updateKitchenRequestModel)
        {
            return Ok(await _cookService.UpdateKitchenDetails(updateKitchenRequestModel));
        }

        /// <summary>
        /// Get all cooks present in the system
        /// </summary>
        /// <returns>cooks ids</returns>
        [HttpGet]
        [Route("cooks")]
        [ProducesResponseType(200, Type = typeof(GetAllCooksResponse))]
        public async Task<IActionResult> GetAllCooks([FromQuery] PageRequestModel pageRequestModel)
        {
            return Ok(await _cookService.GetAllCooks(pageRequestModel));
        }
    }
}