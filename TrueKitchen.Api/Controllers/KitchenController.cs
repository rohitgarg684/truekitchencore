﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Api.Middleware.Authorization;
using TrueKitchen.Contracts;
using TrueKitchen.Contracts.Category;
using TrueKitchen.Contracts.Pagination;
using TrueKitchen.Contracts.RecipeModels;
using TrueKitchen.Infra.Api.Middleware.Authorization;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Controllers
{
    /// <summary>
    /// Represents test controller that should be removed.
    /// </summary>
    [Route("kitchen")]
    public class KitchenController : Controller
    {
        private readonly IKitchenService _kitchenService;
        public KitchenController(IKitchenService kitchenService)
        {
            _kitchenService = kitchenService;
        }


        /// <summary>
        /// Any user can look for specific recipe aka dish if he/she knows the recipe_id. Recipe_id is unique and can only belong to a specific cook.
        /// </summary>
        /// <param name="recipeId">recipe id is required whose details we are interested in</param>
        /// <returns></returns>
        [HttpGet]
        [Route("recipes/{recipeId}")]
        [ProducesResponseType(200, Type= typeof(RecipeViewModel))]
        public async Task<IActionResult> GetRecipe(int recipeId)
        {
            var recipe = await _kitchenService.GetRecipeById(recipeId);
            if (recipe == null)
            {
                return NotFound();
            }
            return Ok(recipe);
        }

        /// <summary>
        /// Any user can look at the dishes present in the specific category. This call does not require authentication because its a readonly info. In future this call would be paginated
        /// </summary>
        /// <param name="categoryId">Category_id is required to view the dishes aka recipes in the category</param>
        /// <returns></returns>
        [HttpGet]
        [Route("categories/{categoryId}/recipes")]
        [ProducesResponseType(200, Type = typeof(GetRecipesByCategoryResponse))]
        public async Task<IActionResult> GetRecipesByCategory(int categoryId, [FromQuery] PageRequestModel pageRequestModel)
        {
            var recipes = await _kitchenService.GetRecipesByCategory(categoryId, pageRequestModel);
            return Ok(recipes);
        }

        /// <summary>
        /// Any user can look at the dishes present in the system for the particular cook. This call does not require any authentication because its a public information which is readonly
        /// </summary>
        /// <returns>category_id and description</returns>
        [HttpGet]
        [Route("cooks/{cookId}/recipes")]
        [ProducesResponseType(200, Type =typeof(IList<RecipeViewModel>))]
        public async Task<IActionResult> GetRecipesByCook(Guid cookId)
        {
            var recipes = await _kitchenService.GetRecipesByCook(cookId);
            return Ok(recipes);
        }

        /// <summary>
        /// Any user can look at the categories present in the system. This call does not require any authentication
        /// </summary>
        /// <returns>category_id and description</returns>
        [HttpGet]
        [Route("categories")]
        [ProducesResponseType(200, Type = typeof(IList<GetCategoryResponse>))]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await _kitchenService.GetCategories();
            return Ok(categories);
        }



        /// <summary>
        /// To use this call. Cook needs to login first. Then cook can add dish to a chosen category. 
        /// </summary>
        /// <param name="model">You need to pass the relevant values for the recipe model like name, description, price, alergy paramters, veg/non-veg etc. </param>
        /// <param name="categoryId">You need to pass category under which your dish should belong. No dish can exist without category.</param>
        /// <returns></returns>
        [Authorize(UserPolicyTypes.CookLoggedIn)]
        [HttpPut]
        [Route("categories/{categoryId}/recipe")]
        [ProducesResponseType(200, Type = typeof(RecipeCreatedResponse))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddRecipe([FromBody] AddRecipeRequestModel model, int categoryId)
        {
            return Ok(await _kitchenService.AddRecipe(categoryId,model));
        }


        /// <summary>
        /// Use this method to update an existing recipe of the cook. If you provide  tags or ingredients only then it will update
        /// </summary>
        /// <param name="model"></param>
        /// <param name="recipeId">Recipe id that needs to be updated</param>
        /// <returns></returns>
        [Authorize(UserPolicyTypes.CookLoggedIn)]
        [HttpPost]
        [Route("recipes/{recipeId}")]
        [ProducesResponseType(200,Type = typeof(RecipeCreatedResponse))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> UpdateRecipe([FromBody] SaveRecipeRequestModel model, int recipeId)
        {
            await _kitchenService.UpdateRecipe(model, recipeId);
            return Ok();
        }

        /// <summary>
        /// This would give you the categories present in our system. Each dish of the cook must be part of some category. Only Admin user can create categories
        /// </summary>
        /// <param name="createCategoryRequestModel">name and description of category is required</param>
        /// <returns>category_id will be returned if category got successfully created.</returns>
        [Authorize(UserPolicyTypes.AdminLoggedIn)]
        [HttpPut]
        [Route("category")]
        [ProducesResponseType(200, Type = typeof(CreateCategoryResponse))]
        [ProducesResponseType(400, Type = typeof(IList<Error>))]
        public async Task<IActionResult> CreateCategory([FromBody] CreateCategoryRequestModel createCategoryRequestModel)
        {
            return Ok(await _kitchenService.CreateCategory(createCategoryRequestModel));
        }


        /// <summary>
        /// Delete recipe by recipe_id. Cook need to be loggedIn. A cook can only delete recipes created by him. 
        /// </summary>
        /// <param name="recipeId"></param>
        /// <returns></returns>
        [Authorize(UserPolicyTypes.CookLoggedIn)]
        [HttpDelete]
        [Route("recipes/{recipeId}")]
        [ProducesResponseType(200, Type = typeof(RecipeCreatedResponse))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> DeleteRecipe(int recipeId)
        {
            await _kitchenService.DeleteRecipe(recipeId);
            return Ok();
        }
    }
}
