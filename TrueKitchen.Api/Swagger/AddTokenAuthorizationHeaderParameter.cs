﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace TrueKitchen.Swagger
{
    public class AddTokenAuthorizationHeaderParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var isAuthorizationFitlerPresent = context.ApiDescription.ActionDescriptor
                .FilterDescriptors
                .Where(x => x.Filter is Microsoft.AspNetCore.Mvc.Authorization.AuthorizeFilter)
                .Any();

            if (!isAuthorizationFitlerPresent)
                return;

            operation.Parameters = operation.Parameters ?? new List<IParameter>();
            const string authTokenParamName = "Authorization";

            if(!operation.Parameters.Where(x=>x.Name.Equals(authTokenParamName)).Any())
            {
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = authTokenParamName,
                    In="header",
                    Type="string",
                    Default="Bearer ",
                    Description="Authorization token Paramerter. Please append 'Bearer ' infront of the token as prefix"
                });
            }
        }
    }
}