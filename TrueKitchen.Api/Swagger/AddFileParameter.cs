﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace TrueKitchen.Swagger
{
    public class AddFileParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var action = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;
            var actionName = action.ActionName;
            if (actionName.EndsWith("Photo")) // controller and action name
            {
                operation.Consumes.Add("multipart/form-data");

                var param = new NonBodyParameter
                {
                    Name = "File",
                    In = "formData",
                    Required = true,
                    Type = "file"
                };

                operation.Parameters.Clear();
                operation.Parameters.Add(param);
            }
        }
    }
}