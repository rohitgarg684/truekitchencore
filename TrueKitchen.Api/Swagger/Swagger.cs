﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Reflection;
using System.Xml.XPath;

namespace TrueKitchen.Api.Swagger
{
    public static class Swagger
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c => 
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title= "True Kitchen Rest Api", Version ="v1" });
                c.OperationFilter<TrueKitchen.Swagger.AddFileParameter>();
                c.OperationFilter<TrueKitchen.Swagger.AddTokenAuthorizationHeaderParameter>();
                c.AddXmlComments();
            });
        }

        private static void AddXmlComments(this SwaggerGenOptions options)
        {
            var currentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var xmlFileName =  $"{ Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var path = Path.Combine(currentDirectory, xmlFileName);

            AddOptionsIfPathExist(options, path);
          
        }

        private static bool AddOptionsIfPathExist(SwaggerGenOptions options, string path)
        {
            if (File.Exists(path))
            {
                options.IncludeXmlComments(() => new XPathDocument(path));
                return true;
            }

            return false;
        }
    }
}
