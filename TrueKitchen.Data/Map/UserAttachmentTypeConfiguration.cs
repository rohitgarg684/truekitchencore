﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;


namespace TrueKitchen.Data.Map
{
    public class UserAttachmentTypeConfiguration : IEntityTypeConfiguration<UserAttachment>
    {
        public void Configure(EntityTypeBuilder<UserAttachment> builder)
        {
            builder.ToTable("user_attachment");

            builder.HasKey(x => x.AttachmentId);
             builder.Property(x => x.AttachmentId)
                .HasColumnName("attachment_id");


             builder.Property(x => x.AttachmentName)
                .HasColumnName("attachment_name")
                .IsRequired();

             builder.Property(x => x.CanonicalName)
                .HasColumnName("canonical_name")
                .IsRequired();

             builder.Property(x => x.Description)
                .HasColumnName("attachment_desc");

             builder.Property(x => x.CreatedDate)
                .HasColumnName("created_dt");

             builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired();

        }
    }
}
