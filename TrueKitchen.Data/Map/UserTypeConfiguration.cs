﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("users","core");

            builder.HasKey(x => x.UserId);
             builder.Property(x => x.UserId)
                .HasColumnName("user_id");

            builder.HasDiscriminator<string>("user_type")
                .HasValue<AdminUser>("Admin")
                .HasValue<Cook>("Cook")
                .HasValue<Customer>("Customer");
                
             builder.Property(x => x.Identifier)
                .HasColumnName("identifier")
                .IsRequired();

            builder.OwnsOne(p=>p.FirstName)
                   .Property(x => x.Value)
                .HasColumnName("first_name")
                .IsRequired();

             builder.Property(x => x.MiddleName)
                .HasColumnName("middle_name");

            builder.OwnsOne(p => p.LastName)
                  .Property(x => x.Value)
                .HasColumnName("last_name")
                .IsRequired();

            builder.OwnsOne(p => p.Email)
                   .Property(x => x.Value)
                    .HasColumnName("email")
                    .IsRequired();

            builder.Ignore(x => x.UserType);
              
             builder.Property(x => x.CreateDateTime)
                .HasColumnName("created_dt")
                .IsRequired();

             builder.Property(x => x.UserPasswordId)
                .HasColumnName("password_id")
                .IsRequired();

            builder.HasOne(x => x.UserPassword)
                .WithMany()
                .HasForeignKey(y => y.UserPasswordId);

             builder.Property(x => x.ProfilePhotoId)
                .HasColumnName("profile_photo_id");

            builder.HasOne(x => x.UserAttachment)
                .WithMany()
                .HasForeignKey(y => y.ProfilePhotoId);

             builder.Property(x => x.AddressId)
               .HasColumnName("address_id");

            builder.HasOne(x => x.Address)
                .WithMany()
                .HasForeignKey(y => y.AddressId);


            builder.OwnsOne(p => p.PrimaryPhoneNo)
                  .Property(x => x.Value)
                   .HasColumnName("primary_phone_no")
                   .IsRequired();


             builder.Property(x => x.SecondryPhoneNo)
              .HasColumnName("secondry_phone_no");

             builder.Property(x => x.IsActive)
              .HasColumnName("is_active");

             builder.Property(x => x.IsPrimaryPhoneNoVerified)
              .HasColumnName("is_primary_phone_no_verified");

             builder.Property(x => x.IsSecondryPhoneNoVerified)
             .HasColumnName("is_secondry_phone_no_verified");
        }
    }
}
