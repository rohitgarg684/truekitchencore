﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class UserPasswordTypeConfiguration : IEntityTypeConfiguration<UserPassword>
    {
        public void Configure(EntityTypeBuilder<UserPassword> builder)
        {

             builder.Property(x => x.NoOfRetires)
                .HasColumnName("no_of_retries");

             builder.Property(x => x.isUserLocked)
                .HasColumnName("is_user_locked");
        }
    }
}
