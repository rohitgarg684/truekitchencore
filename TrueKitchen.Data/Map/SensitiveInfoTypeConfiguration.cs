﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class SensitiveInfoTypeConfiguration : IEntityTypeConfiguration<SensitiveInfo>
    {
        public void Configure(EntityTypeBuilder<SensitiveInfo> builder)
        {
            builder.ToTable("sensitive_info","core");

            builder.HasKey(x => x.SensitiveInfoId);
            builder.Property(x => x.SensitiveInfoId)
               .HasColumnName("sensitive_info_id");

            builder.HasDiscriminator<string>("sensitive_info_type")
                .HasValue<UserAadharCard>("Aadhar")
                .HasValue<UserPassword>("UserPassword");

            builder.Property(x => x.Hash)
               .HasColumnName("hash");

            builder.Property(x => x.Entropy)
               .HasColumnName("entropy");

            builder.Property(x => x.EncryptedValue)
               .HasColumnName("encrypted_value");

            builder.Property(x => x.Value)
               .HasColumnName("value");

            builder.Property(x => x.CreateDateTime)
               .HasColumnName("created_dt");
        }
    }
}
