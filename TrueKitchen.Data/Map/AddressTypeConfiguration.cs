﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class AddressTypeConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("address");

            builder.HasKey(x => x.AddressId);
            builder.Property(x => x.AddressId)
                .HasColumnName("address_id");

            builder.Property(x => x.AddressLine1)
                .HasColumnName("line1");

           builder.Property(x => x.AddressLine2)
                .HasColumnName("line2");

            builder.Property(x => x.State)
                .HasColumnName("state");

            builder.Property(x => x.ZipCode)
                .HasColumnName("zip_code");

            builder.Property(x => x.Country)
                .HasColumnName("country");

            builder.Property(x => x.Latitude)
               .HasColumnName("latitude");

            builder.Property(x => x.Longitude)
               .HasColumnName("longitude");
        }
    }
}
