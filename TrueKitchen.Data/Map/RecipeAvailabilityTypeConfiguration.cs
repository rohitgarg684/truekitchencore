﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class RecipeAvailabilityTypeConfiguration : IEntityTypeConfiguration<RecipeAvailability>
    {
        public void Configure(EntityTypeBuilder<RecipeAvailability> builder)
        {
            builder.Property(x => x.RecipeId)
             .HasColumnName("recipe_id")
             .IsRequired();

            builder.HasOne(x => x.Recipe)
           .WithMany(y => y.RecipeAvailabilities)
           .HasForeignKey(y => y.RecipeId)
           .OnDelete(DeleteBehavior.Cascade)
           .IsRequired();
        }
    }
}

