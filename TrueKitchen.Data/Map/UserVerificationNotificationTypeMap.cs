﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class UserVerificationNotificationTypeMap : IEntityTypeConfiguration<UserVerificationNotification>
    {
        public void Configure(EntityTypeBuilder<UserVerificationNotification> builder)
        {
            builder.ToTable("user_verification_notificatons");

            builder.HasKey(x => x.UserVerificationNotificationId);
             builder.Property(x => x.UserVerificationNotificationId)
                .HasColumnName("notification_id");

             builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired();

            builder.HasOne(x => x.User)
               .WithMany()
               .HasForeignKey(x => x.UserId);

             builder.Property(x => x.VerificationMode)
                .HasColumnName("verification_mode")
                .IsRequired();

             builder.Property(x => x.VerificationEntityValue)
                .HasColumnName("verification_entity_value")
                .IsRequired();

             builder.Property(x => x.VerificationFor)
                .HasColumnName("verification_for")
                .IsRequired();

             builder.Property(x => x.VerificationCode)
                .HasColumnName("verification_code")
                .IsRequired();

             builder.Property(x => x.CreatedDate)
                .HasColumnName("created_date")
                .IsRequired();

             builder.Property(x => x.ExpiryDate)
               .HasColumnName("expiration_date")
               .IsRequired();

             builder.Property(x => x.IsActive)
              .HasColumnName("is_active")
              .IsRequired();

             builder.Property(x => x.IsVerified)
              .HasColumnName("is_verified")
              .IsRequired();
        }
    }
}
