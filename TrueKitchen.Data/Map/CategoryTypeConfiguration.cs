﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class CategoryTypeConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("category");

            builder.HasKey(x => x.CategoryId);
             builder.Property(x => x.CategoryId)
                .HasColumnName("category_id")
                .IsRequired();

             builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

             builder.Property(x => x.Description)
                .HasColumnName("description")
                .IsRequired();
        }
    }
}
