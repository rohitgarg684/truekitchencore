﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;


namespace TrueKitchen.Data.Map
{
    public class RecipeTagTypeConfiguration: IEntityTypeConfiguration<RecipeTag>
    {
        public void Configure(EntityTypeBuilder<RecipeTag> builder)
        {
            builder.ToTable("recipe_tag");

            builder.HasKey(x => x.RecipeTagId);

            builder.Property(x => x.RecipeTagId)
              .HasColumnName("recipe_tag_id");

            builder.Property(x => x.RecipeId)
               .HasColumnName("recipe_id");

            builder.Property(x => x.TagName)
               .HasColumnName("tag_name");

            builder.HasOne(x => x.Recipe)
                .WithMany(y => y.RecipeTags)
                .HasForeignKey(x => x.RecipeId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired(); 
        }
    }
}
