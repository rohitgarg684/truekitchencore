﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class NutritionTypeConfiguration : IEntityTypeConfiguration<Nutrition>
    {
        public void Configure(EntityTypeBuilder<Nutrition> builder)
        {
            builder.ToTable("nutrition");

            builder.HasKey(x => x.NutritionId);
            builder.Property(x => x.NutritionId)
                .HasColumnName("nutrition_id");

            builder.Property(x => x.NutritionName)
                .HasColumnName("nutrition_name");
        }
    }
}
