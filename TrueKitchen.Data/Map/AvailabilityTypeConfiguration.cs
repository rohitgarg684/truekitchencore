﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Domain.Enum;

namespace TrueKitchen.Data.Map
{
    public class AvailabilityTypeConfiguration : IEntityTypeConfiguration<Availability>
    {
        public void Configure(EntityTypeBuilder<Availability> builder)
        {
            builder.ToTable("availability");

            builder.HasKey(x => x.RecipeAvailabilityId);
            builder.Property(x => x.RecipeAvailabilityId)
               .HasColumnName("availability_id");

            builder.HasDiscriminator<string>("availability_type")
                .HasValue<RecipeAvailability>("Recipe")
                .HasValue<KitchenAvailability>("Kitchen");

            builder.Property(x => x.StartTime)
               .HasColumnName("start_time")
               .IsRequired();

            builder.Property(x => x.EndTime)
               .HasColumnName("end_time")
               .IsRequired();


         
            var converter = new ValueConverter<DayOfTheWeek, string>(
                 v => v.ToString(),
                 v => (DayOfTheWeek)Enum.Parse(typeof(DayOfTheWeek), v));

            builder.Property(x => x.DayOfTheWeek)
               .HasConversion(converter)
               .HasColumnName("day_of_the_week");
        }
    }
}
