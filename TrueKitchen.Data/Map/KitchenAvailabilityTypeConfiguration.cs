﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class KitchenAvailabilityTypeConfiguration : IEntityTypeConfiguration<KitchenAvailability>
    {
        public void Configure(EntityTypeBuilder<KitchenAvailability> builder)
        {
            builder.Property(x => x.CookId)
             .HasColumnName("cook_id")
             .IsRequired();

            builder.HasOne(x => x.Cook)
               .WithMany(y => y.KitchenAvailabilities)
               .HasForeignKey(y => y.CookId);
        }
    }
}
