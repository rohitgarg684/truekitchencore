﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class AdminUserTypeConfiguration : IEntityTypeConfiguration<AdminUser>
    {
        public void Configure(EntityTypeBuilder<AdminUser> builder)
        {

            builder.Property(x => x.UserCreationReason)
                .HasColumnName("user_creation_reason");
        }
    }
}
