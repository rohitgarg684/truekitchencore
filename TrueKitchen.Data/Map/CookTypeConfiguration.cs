﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class CookTypeConfiguration : IEntityTypeConfiguration<Cook>
    {
        public void Configure(EntityTypeBuilder<Cook> builder)
        {
            builder.Property(x => x.KitchenName)
               .HasColumnName("kitchen_name");

            builder.Property(x => x.KitchenDescription)
               .HasColumnName("kitchen_description");

            builder.Property(x => x.UserAadharCardId)
               .HasColumnName("aadhar_id");

            builder.HasOne(x => x.UserAadharCard)
                .WithMany()
                .HasForeignKey(y => y.UserAadharCardId);

            builder.Property(x => x.IsKitchenAvailable)
              .HasColumnName("is_kitchen_available");
        }
    }
}
