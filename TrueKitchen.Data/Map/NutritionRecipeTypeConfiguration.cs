﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class NutritionRecipeTypeConfiguration : IEntityTypeConfiguration<NutritionRecipe>
    {
        public void Configure(EntityTypeBuilder<NutritionRecipe> builder)
        {
            builder.ToTable("recipe_nutritions");

            builder.HasKey(x => x.NutritionRecipeId);
            builder.Property(x => x.NutritionRecipeId)
               .HasColumnName("nutrition_recipe_id");

            builder.Property(x => x.RecipeId)
               .HasColumnName("recipe_id")
               .IsRequired();

            builder.HasOne(x => x.Recipe)
                .WithMany()
                .HasForeignKey(y => y.RecipeId);

            builder.Property(x => x.NutritionId)
               .HasColumnName("nutrition_id")
               .IsRequired();

            builder.HasOne(x => x.Nutrition)
                .WithMany()
                .HasForeignKey(y => y.NutritionId);

            builder.Property(x => x.NutritionValue)
               .HasColumnName("nutrition_value");

            builder.Property(x => x.NutritionMeasingUnit)
               .HasColumnName("nutrition_unit");
        }
    }
}
