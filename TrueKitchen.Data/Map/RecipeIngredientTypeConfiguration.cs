﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class RecipeIngredientTypeConfiguration : IEntityTypeConfiguration<RecipeIngredient>
    {
        public void Configure(EntityTypeBuilder<RecipeIngredient> builder)
        {
            builder.ToTable("recipe_ingredient");

            builder.HasKey(x => x.RecipeIngredientId);
            builder.Property(x => x.RecipeIngredientId)
               .HasColumnName("recipe_ingredient_id");

            builder.Property(x => x.Name)
               .HasColumnName("ingredient_name");

            builder.Property(x => x.RecipeId)
               .HasColumnName("recipe_id");

            builder.HasOne(x => x.Recipe)
               .WithMany(y => y.RecipeIngredients)
               .HasForeignKey(y => y.RecipeId)
               .OnDelete(DeleteBehavior.Cascade)
               .IsRequired();
        }
    }
}
