﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Map
{
    public class RecipeTypeConfiguration : IEntityTypeConfiguration<Recipe>
    {
        public void Configure(EntityTypeBuilder<Recipe> builder)
        {
            builder.ToTable("recipe");

            builder.HasKey(x => x.RecipeId);
             builder.Property(x => x.RecipeId)
                .HasColumnName("recipe_id");

             builder.Property(x => x.RecipeName)
                .HasColumnName("name")
                .IsRequired();

             builder.Property(x => x.Description)
                .HasColumnName("description")
                .IsRequired();

             builder.Property(x => x.IsVeg)
                .HasColumnName("is_veg")
                .IsRequired();

             builder.Property(x => x.Price)
                .HasColumnName("price")
                .IsRequired();

             builder.Property(x => x.ServingSize)
                .HasColumnName("serving_size");

             builder.Property(x => x.IsContainingDairy)
                .HasColumnName("is_containing_dairy");

             builder.Property(x => x.IsContainingNuts)
                .HasColumnName("is_containing_nuts");

             builder.Property(x => x.IsVegan)
                .HasColumnName("is_vegan");

             builder.Property(x => x.CookId)
                .HasColumnName("cook_id")
                .IsRequired();

            builder.HasOne(x => x.Cook)
                .WithMany(y=>y.Recipes)
                .HasForeignKey(y => y.CookId)
                .OnDelete(DeleteBehavior.Cascade);

             builder.Property(x => x.CategoryId)
                .HasColumnName("category_id")
                .IsRequired();

            builder.HasOne(x => x.Category)
                .WithMany()
                .HasForeignKey(y => y.CategoryId);

            builder.Property(x => x.IsRecipeAvailable)
                .HasColumnName("is_recipe_available")
                .IsRequired();

            builder.Property(x => x.PreparationTimeInMin)
               .HasColumnName("prep_time_min")
               .IsRequired();

        }
    }
}

