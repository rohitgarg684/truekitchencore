﻿using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.ErrorCodes.User;
using TrueKitchen.Domain.Services.User.BaseUser;
using Truekitchen.Infra.Domain.Validation;
using Microsoft.EntityFrameworkCore;

namespace TrueKitchen.Data.Repository.User.Validations
{
    public class UserValidationService  : IUserValidationService
    {
        private readonly IKitchenContext _kitchenContext;
        public UserValidationService(IKitchenContext dbContext)
        {
            _kitchenContext = dbContext;
        }

        public async Task<ValidationResponse> ValidateEmail(Domain.Entities.User user)
        {
            var validationResponse = new ValidationResponse();
            var isEmailIdPresent = await _kitchenContext.Users.AsQueryable().FirstOrDefaultAsync(x => x.Email.Value == user.Email.Value && x.UserId != user.UserId) != null;
            if (isEmailIdPresent)
            {
                validationResponse.AddError(UserWithGivenEmailAlreadyPresent.ErrorCode, UserWithGivenEmailAlreadyPresent.ErrorMessage);
            }

            return validationResponse;
        }

        public async Task<ValidationResponse> ValidatePrimaryPhoneNo(Domain.Entities.User user)
        {
            var validationResponse = new ValidationResponse();
            var isPrimaryPhoneNoPresent = await _kitchenContext.Users.AsQueryable().FirstOrDefaultAsync(x => (x.PrimaryPhoneNo.Value == user.PrimaryPhoneNo.Value) && x.UserId != user.UserId) != null;
            if (isPrimaryPhoneNoPresent)
            {
                validationResponse.AddError(UserWithGivenPhoneAlreadyPresent.ErrorCode, UserWithGivenPhoneAlreadyPresent.ErrorMessage(user.PrimaryPhoneNo));
            }
            return validationResponse;
        }
    }
}
