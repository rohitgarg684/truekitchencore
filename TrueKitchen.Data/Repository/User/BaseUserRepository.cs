﻿using Autofac;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Core.Exceptions;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Data.Context;
using TrueKitchen.Data.Service.User;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Domain.ErrorCodes.User;
using TrueKitchen.Domain.Resolver;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Data.Repository.User
{
    public abstract class BaseUserRepository<T>: BaseRepository<T>, IBaseUserRepository<T> where T: Domain.Entities.User
    {
        private readonly Func<string, IResolver> _resolverFactory;
        private readonly IUserPhoneNoVerificationService _userPhoneNoVerificationService;

        protected BaseUserRepository(IKitchenContext dbContext, IComponentContext componentContext) : base(dbContext)
        {
            _resolverFactory = componentContext.Resolve<Func<string, IResolver>>();
            _userPhoneNoVerificationService = componentContext.Resolve<IUserPhoneNoVerificationService>();
        }

        public async Task<T> GetUserByPrimaryPhoneNo(PhoneNo phoneNo)
        {
            return _dbSet.Where(x => x.PrimaryPhoneNo.Value == phoneNo.Value).FirstOrDefault();
        }

        public async Task<ValidationResponse> IsPrimaryPhoneNoAndPasswordValid(PhoneNo phoneNo, string password)
        {
            var validationResponse = new ValidationResponse();
            var user = await GetUserByPrimaryPhoneNo(phoneNo);
            if (user == null)
            {
                validationResponse.AddError(UserPrimaryPhoneNoAndPasswordDoesNotMatch.ErrorCode, UserPrimaryPhoneNoAndPasswordDoesNotMatch.ErrorMessage);
            }
            return validationResponse;
        }

        public async Task<T> CreateUser(T user)
        {
            await base.Add(user);
            await SaveUser(user);
            return user;
        }

        public async Task SendVerificationCode(Guid userIdentifier, PhoneNo phoneNo)
        {
            var user = await GetOne(x => x.Identifier == userIdentifier);
            var validationResponse = new ValidationResponse();
            if(user is null)
            {
                validationResponse.AddError(UserNotFoundWithProvidedIdentifier.ErrorCode, UserNotFoundWithProvidedIdentifier.ErrorMessage(userIdentifier));
                throw new ValidationException(validationResponse);
            }

            validationResponse = await UserVerificationNotification.ValidatePhoneNotification(user, phoneNo);
            if (validationResponse.IsError)
            {
                throw new ValidationException(validationResponse);
            }
            await _userPhoneNoVerificationService.SendPhoneNoVerificationMessage(user.UserId, phoneNo);
        }

        public async Task SaveUser(T user)
        {
            var validationResponse = await user.ValidateUser(_resolverFactory);
            if (validationResponse.IsError)
            {
                throw new ValidationException(validationResponse);
            }
            await Save();
        }

        public async Task VerifyPhoneNoNotificationCode(Guid userIdentifier, PhoneNo phoneNo, string verificationCode)
        {
            var user = await GetOne(x => x.Identifier == userIdentifier);
            var validationResponse = new ValidationResponse();
            if (user is null)
            {
                validationResponse.AddError(UserNotFoundWithProvidedIdentifier.ErrorCode, UserNotFoundWithProvidedIdentifier.ErrorMessage(userIdentifier));
                throw new ValidationException(validationResponse);
            }

            await _userPhoneNoVerificationService.VerifyPhoneNoNotificationCode(user.UserId, phoneNo, verificationCode);
        }
    }
}
