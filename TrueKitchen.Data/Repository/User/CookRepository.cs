﻿using Autofac;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Infra.Core.Exceptions;
using TrueKitchen.Core.Exceptions;
using TrueKitchen.Core.Models.Recipe;
using TrueKitchen.Core.Repository.Interfaces;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Data.Context;
using TrueKitchen.Data.Service.Recipe;
using TrueKitchen.Domain.Entities;
using Truekitchen.Infra.Domain.Pagination;
using Microsoft.EntityFrameworkCore;

namespace TrueKitchen.Data.Repository.User
{
    public class CookRepository : BaseUserRepository<Cook>, ICookRepository
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;
        private readonly IRecipeService _recipeService;

        public CookRepository(IKitchenContext dbContext, ICategoryRepository categoryRepository, IMapper mapper, IComponentContext componentContext) : base(dbContext, componentContext)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
            _recipeService = componentContext.Resolve<IRecipeService>() ;
        }

        public IList<string> Validate(Cook cook)
        {
            return new List<String>();
        }
        
        public async Task<Recipe> AddRecipe(AddRecipeModel addRecipeModel)
        {
            var cook = await GetOne(x => x.UserId == addRecipeModel.CookId);
            var recipe = _mapper.Map<Recipe>(addRecipeModel);

            await ValidateAddRecipe(recipe, cook);
            cook.Recipes.Add(recipe);

            await _recipeService.AddTags(recipe, addRecipeModel.Tags);
            await _recipeService.AddIngrdients(recipe, addRecipeModel.Ingredients);
            await _recipeService.AddRecipeAvailabilities(recipe, addRecipeModel.RecipeAvailabilities);

            await Update(cook);
            await Save();

            return recipe;
        }

        public async Task DeleteRecipe(int cookId, int recipeId)
        {
            var cook = await GetOne(x => x.UserId == cookId);
            await _recipeService.DeleteRecipe(cook, recipeId);

            await Save();
        }
        public async Task UpdateRecipe(UpdateRecipeModel updateRecipeModel)
        {
            var cook = await GetOne(x => x.UserId == updateRecipeModel.CookId);

            BaseException.ThrowIfValidationFails(await Recipe.ValidateExistingRecipeId(cook, updateRecipeModel.RecipeId));
            BaseException.ThrowIfValidationFails(await Recipe.ValidateRecipeAlreadyPresentForCook(cook, updateRecipeModel.RecipeId, updateRecipeModel.RecipeName));

            var existingRecipe = cook.Recipes.First(x => x.RecipeId == updateRecipeModel.RecipeId);
            var recipe = _mapper.Map<UpdateRecipeModel, Recipe>(updateRecipeModel, existingRecipe);


            await _recipeService.MergeTags(recipe, updateRecipeModel.Tags);
            await _recipeService.MergeIngrdients(recipe, updateRecipeModel.Ingredients);
            await _recipeService.MergeRecipeAvailabilities(recipe, updateRecipeModel.RecipeAvailabilities);


            await ValidateRecipe(recipe, cook);
            await Update(cook);
            await Save();
        }

        public async Task<IList<Recipe>> GetRecipes(Guid cookIdentifier)
        {
            var cook = await GetOne(x => x.Identifier == cookIdentifier);
            cook.Recipes.ToList();
            return cook.Recipes.ToList();
        }

        public async Task<PagedResponse<Recipe>> GetRecipesByCategory(int categoryId, PageRequest pageRequest)
        {
            var recipes = await _kitchenContext.Recipes.OrderBy(x => x.CategoryId).Where(x=>x.CategoryId == categoryId).Skip(pageRequest.GetSkip()).Take(pageRequest.GetTake()).ToListAsync();
            int count = await _kitchenContext.Recipes.OrderBy(x => x.CategoryId).Where(x => x.CategoryId == categoryId).AsQueryable().CountAsync();
            return  new PagedResponse<Recipe>(recipes, new PageInfo(count, pageRequest));
        }

        private  async Task ValidateAddRecipe(Recipe recipe, Cook cook)
        {
            BaseException.ThrowIfValidationFails(await Recipe.ValidateRecipeAlreadyPresentForCook(cook, recipe.RecipeId, recipe.RecipeName));
            await ValidateRecipe(recipe, cook);
        }

        private async Task ValidateRecipe(Recipe recipe, Cook cook)
        {
            var isCategoryPresent = await _categoryRepository.Any(x => x.CategoryId == recipe.CategoryId);

            if (!isCategoryPresent)
                throw new CategoryNotPresentException(recipe.CategoryId);

            BaseException.ThrowIfValidationFails(await Recipe.ValidateRecipeName(recipe.RecipeName));
        }


        public async Task<Recipe> GetRecipeById(int recipeId)
        {
            var recipe = _kitchenContext.Recipes.Where(x => x.RecipeId == recipeId).FirstOrDefault();
            return recipe;
        }

        public async Task<PagedResponse<Cook>> GetCooks(PageRequest pageRequest)
        {
            var cooks = await _kitchenContext.Cooks.OrderBy(x => x.UserId).Skip(pageRequest.GetSkip()).Take(pageRequest.GetTake()).ToListAsync();
            int count= await _kitchenContext.Cooks.AsQueryable().CountAsync();
            return new PagedResponse<Cook>(cooks, new PageInfo(count, pageRequest));
        }
    }
}
