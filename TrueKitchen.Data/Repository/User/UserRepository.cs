﻿using Autofac;
using System.Threading.Tasks;
using TrueKitchen.Core.Data.Repository.User;
using TrueKitchen.Core.File;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Domain.File;
using TrueKitchen.Domain.Resolver;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Data.Repository.User
{
    public class UserRepository : BaseUserRepository<Domain.Entities.User>, IUserRepository
    {
        protected readonly IFileRepository _fileRepository;

        public UserRepository(IKitchenContext dbContext, IFileRepository fileRepository, IComponentContext componentContext) : base(dbContext, componentContext)
        {
            _fileRepository = fileRepository;
        }

        public async Task<string> SaveProfilePhoto(FileUploadRequest fileUploadRequest, int userId)
        {
            var user = await GetOne(x => x.UserId == userId);

            if (user.UserAttachment != null)
            {
                await _fileRepository.Delete(user.UserAttachment.CanonicalName);
               _kitchenContext.UserAttachments.Remove(user.UserAttachment);
                
            }

            var fileSaveResponse = await _fileRepository.Save(fileUploadRequest);
            var userAttachment = new UserAttachment
            {
                AttachmentName = fileUploadRequest.FileName,
                CanonicalName = fileSaveResponse.CanonicalName,
                UserId = userId
            };

            user.UserAttachment = userAttachment;
            await Update(user);
            await Save();
            return fileSaveResponse.Url;
        }

        public async Task SaveProfile(Domain.Entities.User user)
        {
            if (user.Address != null)
            {
                if (user.Address.AddressId > 0)
                    _kitchenContext.Address.Update(user.Address);
                else
                    _kitchenContext.Address.Update(user.Address);
            }

            await Update(user);
            await SaveUser(user);
        }
    }
}
