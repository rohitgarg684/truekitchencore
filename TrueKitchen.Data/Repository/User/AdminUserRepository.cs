﻿using Autofac;
using System;
using System.Collections.Generic;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Domain.Resolver;

namespace TrueKitchen.Data.Repository.User
{
    public class AdminUserRepository:BaseUserRepository<AdminUser>,IAdminuserRepository
    {
        public AdminUserRepository(IKitchenContext dbContext, IComponentContext componentContext) : base(dbContext, componentContext)
        {
        }

        public IList<string> Validate(AdminUser adminUser)
        {
            return new List<string> { };
        }
    }
}
