﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using TrueKitchen.Core.Parameters;
using TrueKitchen.Core.Security;
using TrueKitchen.Domain.Security;

namespace TrueKitchen.Domain.Repository.Security
{
    public class TokenRepository: ITokenRepository
    {
        private readonly JwtParameters _jwtParameters;

        public TokenRepository(JwtParameters jwtParameters)
        {
            _jwtParameters = jwtParameters;
        }

        public async Task<GenerateTokenResponse> GenerateToken(string username)
        {
            var symmetricKey = Convert.FromBase64String(_jwtParameters.Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddSeconds(_jwtParameters.TTL),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return new GenerateTokenResponse(token, _jwtParameters.TTL);
        }

        public async Task<TokenValidationResponse> ValidateToken(string token)
        {
            var jwtTokenValidationResponse = new TokenValidationResponse { IsUserValidated = false };

            try
            {
                var simplePrinciple = GetPrincipal(token);
                var identity = simplePrinciple.Identity as ClaimsIdentity;

                if (identity == null)
                    return jwtTokenValidationResponse;

                if (!identity.IsAuthenticated)
                    return jwtTokenValidationResponse;

                var usernameClaim = identity.FindFirst(ClaimTypes.Name);
                jwtTokenValidationResponse.UserName = usernameClaim?.Value;

                if (string.IsNullOrEmpty(jwtTokenValidationResponse.UserName))
                    return jwtTokenValidationResponse;

                // More validate to check whether username exists in system
                jwtTokenValidationResponse.IsUserValidated = true;
            }
            catch
            {
                return new TokenValidationResponse { IsUserValidated = false };
            }

            return jwtTokenValidationResponse;
        }


        private ClaimsPrincipal GetPrincipal(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

            if (jwtToken == null)
                return null;

            var symmetricKey = Convert.FromBase64String(_jwtParameters.Secret);

            var validationParameters = new TokenValidationParameters()
            {
                RequireExpirationTime = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
            };

            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);
            return principal;
        }
    }
}
