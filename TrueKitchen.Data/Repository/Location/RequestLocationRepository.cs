﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using TrueKitchen.Core.Location;
using TrueKitchen.Core.Parameters;
using TrueKitchen.Domain.Location;

namespace TrueKitchen.Data.Location.Repository
{
    public class RequestLocationRepository: IRequestLocationRepository
    {
        private readonly IpStackParameters _ipStackParameters;

        public RequestLocationRepository(IpStackParameters ipStackParameters)
        {
            _ipStackParameters = ipStackParameters;
        }

        public async Task<RequestGeoLocation> GetLocationFromIPAsync(string ipAddress)
        {
            var client = new HttpClient();
            var response = await client.GetStringAsync($"{ _ipStackParameters.Url}/{ipAddress}?access_key={_ipStackParameters}");
            var ipStackGeoResponse = JsonConvert.DeserializeObject<IpStackGeoResponse>(response);
            return new RequestGeoLocation
            {
                City = ipStackGeoResponse.City,
                CountryName = ipStackGeoResponse.CountryName,
                ZipCode = ipStackGeoResponse.ZipCode,
                Latitude = ipStackGeoResponse.Latitude,
                Longitude = ipStackGeoResponse.Longitude,
            };
        }
    }
}
