﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Core.File;
using TrueKitchen.Core.Parameters;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Data.Repository.File
{
    public class ImageFileValidationService: IFileValidationService
    {
        private readonly ImageFileParameters _imageFileParameters;

        public ImageFileValidationService(ImageFileParameters imageFileParameters)
        {
            _imageFileParameters = imageFileParameters;
        }

        public async Task<FileValidationResponse> IsFileValid(FileUploadRequest imageUploadRequest)
        {
            var fileValidationResponse = new FileValidationResponse();
            var isImageMimeType = _imageFileParameters.AllowedImageFileExtensions.Select(x => $"image/{x}").ToArray().Contains(imageUploadRequest.ContentType);

            var fileExtensionValid = !_imageFileParameters.AllowedImageFileExtensions.Contains(Path.GetExtension(imageUploadRequest.FileName).ToLower());
            if (!isImageMimeType)
            {
                return new FileValidationResponse($"Only {string.Join(",", _imageFileParameters.AllowedImageFileExtensions.ToArray())} are allowed", 192);
            }


            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!imageUploadRequest.Stream.CanRead)
                {
                    return new FileValidationResponse("Unable to read file", 192);
                }
                //------------------------------------------
                //check whether the image size exceeding the limit or not
                //------------------------------------------ 
                if (imageUploadRequest.FileSize > _imageFileParameters.ImageMaximumInBytes)
                {
                    return new FileValidationResponse($"File Size is more than {_imageFileParameters.ImageMaximumInMB} MB", 193);
                }

            }
            catch (Exception)
            {
                return new FileValidationResponse("Unable to read file - UnExpected Error", 194);
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------
            try
            {
                using (var bitmap = new System.Drawing.Bitmap(imageUploadRequest.Stream))
                {
                }
            }
            catch (Exception)
            {
                return new FileValidationResponse("Unable to read image File", 195);
            }

            fileValidationResponse.IsValid = true;
            return fileValidationResponse;

        }
    }
}
