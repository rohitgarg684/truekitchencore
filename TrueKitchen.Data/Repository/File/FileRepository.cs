﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Threading.Tasks;
using TrueKitchen.Core.Exceptions;
using TrueKitchen.Core.File;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Data.Repository.File
{
    public class FileRepository:IFileRepository
    {
        private readonly string BucketNameForProfilePhotos = "truekitchenapiuserprofilephotos";
        private static IAmazonS3 _client;
        private readonly IFileValidationService _fileValidationService;

        public FileRepository(IFileValidationService fileValidationService)
        {

            if (_client == null)
                _client = new AmazonS3Client(RegionEndpoint.USEast1);

            _fileValidationService = fileValidationService;
        }

        public async Task<FileSaveResponse> Save(FileUploadRequest fileUploadRequest)
        {
            var imageValidationResponse = await _fileValidationService.IsFileValid(fileUploadRequest);

            if (!imageValidationResponse.IsValid)
                throw new ImageUploadException(imageValidationResponse.Code, imageValidationResponse.Message);

            var photoName = Guid.NewGuid();

            var key = $"{photoName}";
            var uploadRequest = new TransferUtilityUploadRequest();
            uploadRequest.CannedACL = S3CannedACL.PublicRead;
            uploadRequest.InputStream = fileUploadRequest.Stream;
            uploadRequest.BucketName = BucketNameForProfilePhotos;
            uploadRequest.Key = key;

            using (TransferUtility fileTransferUtility = new TransferUtility(_client))
            {
                fileTransferUtility.Upload(uploadRequest);
            }

            return new FileSaveResponse(key, $"https://s3.amazonaws.com/{BucketNameForProfilePhotos}/{key}");

        }

        public async Task Delete(string fileName)
        {
            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = BucketNameForProfilePhotos,
                Key = fileName
            };

            await _client.DeleteObjectAsync(deleteObjectRequest);
        }
    }
}
