﻿using System;
using System.Threading.Tasks;
using TrueKitchen.Core.Exceptions;
using TrueKitchen.Core.Repository.Interfaces;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(IKitchenContext dbContext) : base(dbContext)
        {
        }

        public async Task<Category> GetCategoryByName(string name)
        {
            return await GetOne(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        }

        public async Task CreateCategory(Category category)
        {
            var existingCategory = await GetCategoryByName(category.Name);

            if (existingCategory != null)
            {
                throw new CategoryAlreadyPresentException();
            }

            await Add(category);
            await Save();
        }
    }
}
