﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TrueKitchen.Core.Repository.Interfaces;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Repository
{
    public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IKitchenContext _kitchenContext;
        protected DbSet<T>_dbSet;

        protected BaseRepository(IKitchenContext kitchenContext)
        {
            _kitchenContext = kitchenContext;
            _dbSet = kitchenContext.Instance.Set<T>();
        }
       

        public virtual async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter)
        {
            return  await _dbSet.AsQueryable().Where(filter).ToListAsync();
        }

        public virtual async Task<T> GetOne(Expression<Func<T, bool>> filter)
        {
            return  await _dbSet.Where(filter).FirstOrDefaultAsync();
        }

        public virtual async Task<bool> Any(Expression<Func<T, bool>> filter)
        {
            return await _dbSet.AnyAsync(filter);
        }

        public virtual async Task Add( T entity)
        {
              _dbSet.Add(entity);
        }

        public virtual async Task Delete(T entity)
        {
            if (_kitchenContext.Instance.Entry(entity).State == EntityState.Detached)
            {
               _dbSet.Attach(entity);
            }

            _dbSet.Remove(entity);
        }

        public virtual async Task Update(T entity)
        {
            _dbSet.Attach(entity);
            _kitchenContext.Instance.Entry(entity).State = EntityState.Modified;
        }

        public virtual async Task Save()
        {
              _kitchenContext.Instance.SaveChanges();
        }

        public virtual void Dispose()
        {
            _kitchenContext.Instance.Dispose();
        }
    }
}
