﻿using Autofac;
using TrueKitchen.Data.Context;
using TrueKitchen.Data.Location.Repository;
using TrueKitchen.Data.Repository;
using TrueKitchen.Data.Repository.File;
using TrueKitchen.Data.Repository.User;
using TrueKitchen.Data.Repository.User.Validations;
using TrueKitchen.Domain.Repository.Security;
using Module = Autofac.Module;

namespace TrueKitchen.Data
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<TokenRepository>()
              .AsImplementedInterfaces()
              .SingleInstance();

            builder.RegisterType<KitchenContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ImageFileValidationService>()
              .AsImplementedInterfaces()
              .InstancePerLifetimeScope();

            builder.RegisterType<FileRepository>()
               .AsImplementedInterfaces()
               .InstancePerLifetimeScope();

            builder.RegisterType<RequestLocationRepository>()
            .AsImplementedInterfaces()
            .InstancePerLifetimeScope();

            builder.RegisterType<AdminUserRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Service.Recipe.RecipeService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Service.User.UserPhoneNoVerificationService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<CookRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<CategoryRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserValidationService>()
                .Named<Domain.Resolver.IResolver>(typeof(Domain.Services.User.BaseUser.IUserValidationService).ToString());
        }
    }
}
