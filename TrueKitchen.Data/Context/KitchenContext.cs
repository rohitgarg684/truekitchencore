﻿using Microsoft.EntityFrameworkCore;
using TrueKitchen.Core;
using TrueKitchen.Data.Map;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Context
{
    public class KitchenContext : DbContext,IKitchenContext
    {
        private readonly ConnectionParameters _connectionParameters;
        public KitchenContext(ConnectionParameters connectionParameters)
        {
            _connectionParameters = connectionParameters;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.UseSqlServer(_connectionParameters.ConnectionString);
        }

        public virtual DbSet<RecipeIngredient> RecipeIngredients { get; set; }
        public virtual DbSet<RecipeAvailability>  RecipeAvailabilities { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<RecipeTag> RecipeTags { get; set; }

        public virtual DbSet<UserAttachment> UserAttachments { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<SensitiveInfo> SensitiveInfos { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Cook> Cooks { get; set; }

        public DbContext Instance => this;

        public DbSet<Address> Address { get; set; }
        public DbSet<UserVerificationNotification> UserVerificationNotifications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //complex type support not added yet in EF Core
            //modelBuilder.ApplyConfiguration(new EmailTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new NameTypeConfiguration());

            modelBuilder.ApplyConfiguration(new RecipeTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RecipeTagTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RecipeIngredientTypeConfiguration());

            modelBuilder.ApplyConfiguration(new AvailabilityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RecipeAvailabilityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new KitchenAvailabilityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new SensitiveInfoTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserPasswordTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserAadharCardTypeConfiguration());

            modelBuilder.ApplyConfiguration(new UserAttachmentTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AddressTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserVerificationNotificationTypeMap());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());

            modelBuilder.ApplyConfiguration(new AdminUserTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CookTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerTypeConfiguration());

            modelBuilder.ApplyConfiguration(new CategoryTypeConfiguration());
        
            
        }
    }
}
