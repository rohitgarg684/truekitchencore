﻿using Microsoft.EntityFrameworkCore;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Context
{
    public interface IKitchenContext
    {
        DbSet<RecipeIngredient> RecipeIngredients { get; set; }
        DbSet<RecipeAvailability> RecipeAvailabilities { get; set; }
        DbSet<RecipeTag> RecipeTags { get; set; }
        DbSet<Recipe> Recipes { get; set; }
        DbContext Instance { get; }
        DbSet<UserAttachment> UserAttachments { get; set; }
        DbSet<Address> Address { get; set; }
        DbSet<Cook> Cooks { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserVerificationNotification> UserVerificationNotifications { get; set; }
    }
}
