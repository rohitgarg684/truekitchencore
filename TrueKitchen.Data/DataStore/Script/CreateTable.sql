﻿CREATE TABLE Category
(
category_id INT IDENTITY(1,1) PRIMARY KEY,
name VARCHAR(100),
[description] VARCHAR(200)
);

GO

CREATE TABLE Nutrition
(
nutrition_id INT IDENTITY(1,1) PRIMARY KEY,
name VARCHAR(100),
);

GO

CREATE TABLE recipe_nutritions
(
nutrition_recipe_id INT IDENTITY(1,1) PRIMARY KEY,
recipe_id INT NOT NULL,
nutrition_id INT NOT NULL,
nutrition_value decimal NOT NULL,
nutrition_unit INT NOT NULL
);

CREATE TABLE recipe
(
recipe_id INT IDENTITY(1,1) PRIMARY KEY,
name VARCHAR(200) NOT NULL,
[description] VARCHAR(400),
price decimal NOT NULL,
is_veg BIT NOT NULL,
is_containing_dairy BIT,
is_containing_nuts BIT,
is_vegan BIT,
cook_id INT NOT NULL,
category_id INT NOT NULL,
serving_size INT,
is_recipe_available BIT NOT NULL DEFAULT 1,
prep_time_min INT 
);
 
CREATE TABLE recipe_ingredient
(
recipe_ingredient_id INT IDENTITY(1,1) PRIMARY KEY,
recipe_id INT NOT NULL,
ingredient_name NVARCHAR(200) NOT NULL,
);

CREATE TABLE [availability]
(
availability_id INT IDENTITY(1,1) PRIMARY KEY,
availability_type VARCHAR(100) NOT NULL,
recipe_id INT ,
start_time TIME NOT NULL,
end_time TIME NOT NULL,
day_of_the_week VARCHAR(100) NOT NULL,
cook_id INT
);


CREATE TABLE recipe_tag
(
recipe_tag_id INT IDENTITY(1,1) PRIMARY KEY,
recipe_id INT,
tag_name NVARCHAR(100),
);


CREATE TABLE [core].[USERS]
(
[user_id] INT IDENTITY(1,1) PRIMARY KEY,
user_type VARCHAR(200),
identifier uniqueidentifier ,
first_name VARCHAR(100) NOT NULL,
middle_name VARCHAR(100),
last_name VARCHAR(100),
email VARCHAR(100),
primary_phone_no VARCHAR(20),
secondry_phone_no VARCHAR(20),
address_id INT,
password_id INT,
profile_photo_id INT, 
is_active BIT DEFAULT 0,
is_primary_phone_no_verified BIT DEFAULT 0,
is_secondry_phone_no_verified BIT DEFAULT 0,
created_dt DATETIME,
user_creation_reason VARCHAR(200),
kitchen_name VARCHAR(100),
kitchen_description VARCHAR(200),
aadhar_id INT,
is_kitchen_available BIT DEFAULT 0,
);


CREATE TABLE [core].sensitive_info
(
[sensitive_info_id] INT IDENTITY(1,1) PRIMARY KEY,
[sensitive_info_type] VARCHAR(200) NOT NULL,
[hash] VARCHAR(200),
[entropy] VARBINARY(16),
[encrypted_value] VARCHAR(200),
[value] VARCHAR(200),
created_dt DATETIME2,
no_of_retries INT,
is_user_locked BIT,
);


GO

CREATE TABLE user_attachment
(
attachment_id INT IDENTITY(1,1) PRIMARY KEY,
attachment_name NVARCHAR(400) NOT NULL,
canonical_name varchar (200) NOT NULL,
attachment_desc vARCHAR(4000),
created_dt DATETIME DEFAULT GETUTCDATE(),
[user_id] INT NOT NULL
);
GO

CREATE TABLE [address]
(
address_id INT IDENTITY(1,1) PRIMARY KEY,
line1 NVARCHAR(400) NOT NULL,
line2 Nvarchar (400),
[state] NVARCHAR (400) NOT NULL,
zip_code  NVARCHAR(50) NOT NULL,
country  NVARCHAR(20) NOT NULL,
latitude VARCHAR(50),
longitude VARCHAR(50),
);
GO


create TABLE user_verification_notificatons
(
 [notification_id] INT IDENTITY(1,1) PRIMARY KEY,
 [user_id] INT NOT NULL,
 [verification_mode] VARCHAR(20) NOT NULL,
 [verification_entity_value] VARCHAR(200) NOT NULL,
 [verification_for] VARCHAR(200) NOT NULL,
 [verification_code] VARCHAR (20) NOT NULL,
 created_date DATETIME NOT NULL,
 expiration_date DATETIME NOT NULL,
 is_active BIT NOT NULL,
 is_verified BIT NOT NULL
);

GO