﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Core.Models.Recipe;
using TrueKitchen.Data.Context;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Data.Service.Recipe
{
    internal class RecipeService: IRecipeService
    {
        private readonly IKitchenContext _kitchenContext;
        private readonly IMapper _mapper;
        
        public RecipeService(IKitchenContext dbContext, IMapper mapper)
        {
            _kitchenContext = dbContext;
            _mapper = mapper;
        }

        public async Task AddTags(Domain.Entities.Recipe recipe, IList<TagModel> tags)
        {
            if (tags != null)
            {
                var existingTags = recipe.RecipeTags.ToList();
                foreach (var tag in tags)
                {
                    if (!existingTags.Any(x => x.TagName == tag.TagName))
                    {
                        var recipeTag = new Domain.Entities.RecipeTag(tag.TagName, recipe.RecipeId);
                        recipe.RecipeTags.Add(recipeTag);
                        _kitchenContext.RecipeTags.Add(recipeTag);
                    }
                }
            }
        }

        public async Task DeleteTags(Domain.Entities.Recipe recipe, IList<TagModel> tags)
        {
            if (tags != null)
            {
                var existingTags = recipe.RecipeTags.ToList();
                foreach (var recipeTag in existingTags)
                {
                    if (!tags.Has(recipeTag.TagName))
                    {
                        recipe.RecipeTags.Remove(recipeTag);
                        _kitchenContext.RecipeTags.Remove(recipeTag);
                    }
                }
            }
        }

        public async Task MergeTags(Domain.Entities.Recipe recipe, IList<TagModel> tags)
        {
            await DeleteTags(recipe, tags);
            await AddTags(recipe, tags);
        }

        public async Task AddIngrdients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients)
        {
            if (ingredients != null)
            {
                var existingIngredients = recipe.RecipeIngredients.ToList();
                foreach (var ing in ingredients)
                {
                    if (!existingIngredients.Any(x => x.Name == ing.IngredientName))
                    {
                        var recipeIng = new Domain.Entities.RecipeIngredient(ing.IngredientName, recipe.RecipeId);
                        recipe.RecipeIngredients.Add(recipeIng);
                        _kitchenContext.RecipeIngredients.Add(recipeIng);
                    }
                }
            }
        }


        public async Task DeleteIngredients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients)
        {
            if (ingredients != null)
            {
                var existingIngredients = recipe.RecipeIngredients.ToList();
                foreach (var recipeIngredient in existingIngredients)
                {
                    if (!ingredients.Has(recipeIngredient.Name))
                        _kitchenContext.RecipeIngredients.Remove(recipeIngredient);
                }
            }
        }

        public async Task MergeIngrdients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients)
        {
            await DeleteIngredients(recipe, ingredients);
            await AddIngrdients(recipe, ingredients);
        }

        public async Task AddRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilityModels)
        {
            if (recipeAvailabilityModels != null)
            {
                var existingAvailabilities = recipe.RecipeAvailabilities.ToList();
                foreach (var availabilityModel in recipeAvailabilityModels)
                {
                    var recipeAvailability = _mapper.Map<RecipeAvailability>(availabilityModel);
                    recipeAvailability.RecipeId = recipe.RecipeId;
                    if (!existingAvailabilities.Contains(recipeAvailability))
                    {
                         recipe.RecipeAvailabilities.Add(recipeAvailability);
                        _kitchenContext.RecipeAvailabilities.Add(recipeAvailability);
                    }
                }
            }
        }

        public async Task DeleteRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilityModels)
        {
            if (recipeAvailabilityModels != null)
            {
                var existingAvailabilities = recipe.RecipeAvailabilities.ToList();
                
                foreach (var availability in existingAvailabilities)
                {
                    if (!recipeAvailabilityModels.Has(availability))
                        _kitchenContext.RecipeAvailabilities.Remove(availability);
                }
            }
        }

        public async Task MergeRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilityModels)
        {
            await DeleteRecipeAvailabilities(recipe, recipeAvailabilityModels);
            await AddRecipeAvailabilities(recipe, recipeAvailabilityModels);
        }

        public async Task DeleteRecipe(Cook cook, int recipeId)
        {
            BaseException.ThrowIfValidationFails(await Domain.Entities.Recipe.ValidateExistingRecipeId(cook, recipeId));
            var recipe = await _kitchenContext
                .Recipes
                .Include(x => x.RecipeAvailabilities)
                .Include(x => x.RecipeIngredients)
                .Include(x => x.RecipeTags)
                .FirstAsync(x => x.RecipeId == recipeId);

            _kitchenContext.Recipes.Remove(recipe);
            cook.Recipes.Remove(recipe);
        }

    }
}
