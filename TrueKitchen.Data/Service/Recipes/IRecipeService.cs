﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Core.Models.Recipe;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Data.Service.Recipe
{
    internal interface IRecipeService
    {
        Task AddTags(Domain.Entities.Recipe recipe, IList<TagModel> tags);
        Task DeleteTags(Domain.Entities.Recipe recipe, IList<TagModel> tags);
        Task MergeTags(Domain.Entities.Recipe recipe, IList<TagModel> tags);
        Task AddIngrdients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients);
        Task DeleteIngredients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients);
        Task MergeIngrdients(Domain.Entities.Recipe recipe, IList<IngredientModel> ingredients);
        Task AddRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilities);
        Task DeleteRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilityModels);
        Task MergeRecipeAvailabilities(Domain.Entities.Recipe recipe, IList<RecipeAvailabilityModel> recipeAvailabilityModels);
        Task DeleteRecipe(Cook cook, int recipeId);
    }
}
