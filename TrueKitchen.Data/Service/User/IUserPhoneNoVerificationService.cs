﻿using System.Threading.Tasks;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Data.Service.User
{
    internal interface IUserPhoneNoVerificationService
    {
        Task SendPhoneNoVerificationMessage(int userId, PhoneNo phoneNo);
        Task VerifyPhoneNoNotificationCode(int userId, PhoneNo phoneNo, string verificationCode);
    }
}
