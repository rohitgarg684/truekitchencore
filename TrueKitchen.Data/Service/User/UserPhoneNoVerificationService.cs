﻿using TrueKitchen.Data.Context;
using System.Linq;
using TrueKitchen.Domain.Enum;
using TrueKitchen.Core.Parameters;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.ErrorCodes.User;
using System;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using TrueKitchen.Domain.Entities;
using Amazon;
using System.Threading.Tasks;
using TrueKitchen.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;

namespace TrueKitchen.Data.Service.User
{
    internal class UserPhoneNoVerificationService : IUserPhoneNoVerificationService
    {
        private readonly IKitchenContext _kitchenContext;
        private readonly MobileNoVerificationParameters _mobileNoVerificationParameters;
        private readonly AmazonSimpleNotificationServiceClient _amazonSimpleNotificationServiceClient;

        public UserPhoneNoVerificationService(IKitchenContext dbContext, MobileNoVerificationParameters mobileNoVerificationParameters)
        {
            _kitchenContext = dbContext;
            _mobileNoVerificationParameters = mobileNoVerificationParameters;
            if (_amazonSimpleNotificationServiceClient == null)
                _amazonSimpleNotificationServiceClient = new AmazonSimpleNotificationServiceClient(RegionEndpoint.USEast1);
        }



        public async Task SendPhoneNoVerificationMessage(int userId, PhoneNo phoneNo)
        {

            var user = _kitchenContext.Users.First(x => x.UserId == userId);
            var phoneNoAlreadyVerifiedValidationResponse = user.ValidatePhoneNoAlreadyVerified(phoneNo);
            if (phoneNoAlreadyVerifiedValidationResponse.IsError)
            {
                throw new ValidationException(phoneNoAlreadyVerifiedValidationResponse);
            }

            var userVerificationNotifications = _kitchenContext.UserVerificationNotifications.Where(x => x.VerificationMode == UserVerificationMode.Mobile && x.VerificationFor == phoneNo.Value).ToList();
            var validationResponse = new ValidationResponse();
            if (userVerificationNotifications.Count() >= _mobileNoVerificationParameters.MaximumVerificationSMSAllowedinLifeTime)
            {
                validationResponse.AddError(UserPhoneNoVerificationLifeTimeError.ErrorCode, UserPhoneNoVerificationLifeTimeError.ErrorMessage(_mobileNoVerificationParameters.MaximumVerificationSMSAllowedinLifeTime));
                throw new ValidationException(validationResponse);
            }

            if (userVerificationNotifications.Count() > _mobileNoVerificationParameters.MaximumVerificationSMSAllowedForInterval)
            {
                var notifications = userVerificationNotifications.OrderBy(x => x.CreatedDate).Take(_mobileNoVerificationParameters.MaximumVerificationSMSAllowedForInterval).ToArray();

                double totalTimeDiff = 0;
                Enumerable.Range(1, notifications.Count())
                    .ToList()
                    .ForEach(
                    i =>
                    totalTimeDiff += (notifications[i].CreatedDate - notifications[i - 1].CreatedDate).Hours);

                totalTimeDiff += (DateTime.UtcNow - notifications[0].CreatedDate).TotalHours;

                if (totalTimeDiff < _mobileNoVerificationParameters.IntervalDurationInHours)
                {
                    validationResponse.AddError(UserPhoneNoVerificationIntervalError.ErrorCode, UserPhoneNoVerificationIntervalError.ErrorMessage(_mobileNoVerificationParameters.IntervalDurationInHours));
                    throw new ValidationException(validationResponse);
                }
            }
            var userVerificationNotification = new UserVerificationNotification
            {
                UserId = userId,
                VerificationMode = UserVerificationMode.Mobile,
                VerificationEntityValue = phoneNo.Value,
                VerificationFor = UserVerificationFor.CreateUser,
                VerificationCode = UserVerificationNotification.GenerateCode(),
                IsActive = true,
            };

            userVerificationNotification.ExpiryDate = userVerificationNotification.CreatedDate.AddHours(_mobileNoVerificationParameters.OTPExpirationTimeInterval);
            _kitchenContext.UserVerificationNotifications.Add(userVerificationNotification);
            await _kitchenContext.Instance.SaveChangesAsync();

            var pubRequest = new PublishRequest();
            pubRequest.Message = $"Your TrueKitchen code is {userVerificationNotification.VerificationCode}";
            pubRequest.PhoneNumber = userVerificationNotification.VerificationEntityValue;
            PublishResponse pubResponse = await _amazonSimpleNotificationServiceClient.PublishAsync(pubRequest);
        }

        public async Task VerifyPhoneNoNotificationCode(int userId, PhoneNo phoneNo, string verificationCode)
        {
            var validationResponse = new ValidationResponse();

            var user = _kitchenContext.Users.First(x => x.UserId == userId);
            var phoneNoAlreadyVerifiedValidationResponse = user.ValidatePhoneNoAlreadyVerified(phoneNo);
            if (phoneNoAlreadyVerifiedValidationResponse.IsError)
            {
                throw new ValidationException(phoneNoAlreadyVerifiedValidationResponse);
            }

            var usernotifications = await _kitchenContext.UserVerificationNotifications
                .Where(x => x.UserId == userId
                         && x.VerificationMode == UserVerificationMode.Mobile
                         && x.VerificationEntityValue == phoneNo.Value
                         && x.IsActive).ToListAsync();

           
            if (!usernotifications.Any())
            {
                validationResponse.AddError(NoActiveUserNotificationFound.ErrorCode, NoActiveUserNotificationFound.ErrorMessage(phoneNo.Value));
                throw new ValidationException(validationResponse);
            }

            if(!usernotifications.Any(x => x.ExpiryDate < DateTime.UtcNow))
            {
                validationResponse.AddError(UserNotificationCodeExpired.ErrorCode, UserNotificationCodeExpired.ErrorMessage(phoneNo.Value));
                throw new ValidationException(validationResponse);
            }

            var notification = usernotifications.FirstOrDefault(x => x.VerificationCode == verificationCode);
            if (notification==null)
            {
                validationResponse.AddError(UserVerificationCodeMismatch.ErrorCode, UserVerificationCodeMismatch.ErrorMessage(phoneNo.Value, verificationCode));
                throw new ValidationException(validationResponse);
            }

            notification.IsActive = false;
            notification.IsVerified = true;
            if(user.PrimaryPhoneNo.Value== phoneNo.Value)
            {
                user.IsPrimaryPhoneNoVerified = true;
            }
            else if (user.SecondryPhoneNo == phoneNo.Value)
            {
                user.IsSecondryPhoneNoVerified = true;
            }

            await _kitchenContext.Instance.SaveChangesAsync();
        }
    }
}
