﻿using System.Threading.Tasks;
using TrueKitchen.Domain.Security;

namespace TrueKitchen.Core.Security
{
    public interface ITokenRepository
    {
        Task<GenerateTokenResponse> GenerateToken(string username);
        Task<TokenValidationResponse> ValidateToken(string token);
    }
}
