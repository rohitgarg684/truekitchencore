﻿using System.Threading.Tasks;

namespace TrueKitchen.Core.Security
{
    public interface ITokenService
    {
        Task<GenerateTokenServiceResponse> GenerateTokenAsync(string username);
        Task<TokenValidationServiceResponse> ValidateToken(string token);
    }
}
