﻿namespace TrueKitchen.Core.Security
{
    public class GenerateTokenServiceResponse
    {
        public GenerateTokenServiceResponse(string token, int ttl)
        {
            Token = token;
            TTL = ttl;
        }

        public string Token { get; set; }
        public int TTL { get; set; }
    }
}
