﻿namespace TrueKitchen.Core.Security
{
    public class TokenValidationServiceResponse
    {
        public TokenValidationServiceResponse(bool isUserValidated, string username)
        {
            IsUserValidated = isUserValidated;
            UserName = username;
        }

        public string UserName { get; set; }
        public bool IsUserValidated { get; set; }
    }
}
