﻿namespace TrueKitchen.Core.Parameters
{
    public class JwtParameters:IAppSettings
    {
        public string Secret { get; set; }
        public int TTL { get; set; }
    }
}
