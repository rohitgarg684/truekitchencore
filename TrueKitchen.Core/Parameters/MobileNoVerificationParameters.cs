﻿namespace TrueKitchen.Core.Parameters
{
    public class MobileNoVerificationParameters
    {
        public int MaximumVerificationSMSAllowedinLifeTime { get; set; }
        public int MaximumVerificationSMSAllowedForInterval { get; set; }
        public int IntervalDurationInHours { get; set; }
        public int MaximumSMSAllowedPerInterval { get; set; }
        public int OTPExpirationTimeInterval { get; set; }
    }
}
