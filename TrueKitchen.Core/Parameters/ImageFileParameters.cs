﻿using System.Collections.Generic;

namespace TrueKitchen.Core.Parameters
{
    public class ImageFileParameters
    {
        public ImageFileParameters()
        {
            AllowedImageFileExtensions = new List<string>();
        }

        public int ImageMaximumInMB { get; set; }
        public int ImageMaximumInBytes { get { return ImageMaximumInMB * 1024 * 1024; } }
        public IList<string> AllowedImageFileExtensions { get; set; }
    }
}
