﻿namespace TrueKitchen.Core.Parameters
{
    public class IpStackParameters : IAppSettings
    {
        public string Url { get; set; }
        public string AccessKey { get; set; }
    }
}
