﻿using System.Threading.Tasks;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Core.File
{
    public interface IFileValidationService
    {
        Task<FileValidationResponse> IsFileValid(FileUploadRequest imageUploadRequest);
    }
}
