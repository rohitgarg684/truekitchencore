﻿using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Core.Exceptions
{
    public class ImageUploadException:BaseException
    {
        public ImageUploadException(int code, string message) : base(code, message, System.Net.HttpStatusCode.BadRequest) { }
    }
}
