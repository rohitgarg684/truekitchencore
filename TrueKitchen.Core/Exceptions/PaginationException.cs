﻿using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Core.Exceptions
{
    public class PaginationException:BaseException
    {
        public PaginationException(string message) : base(196, message, System.Net.HttpStatusCode.BadRequest) { }
    }
}
