﻿using System.Net;
using TrueKitchen.Domain.ErrorCodes;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Core.Exceptions
{
    public class CategoryAlreadyPresentException : BaseException
    {
        public CategoryAlreadyPresentException() : base(CategoryAlreadyPresent.ErrorCode, CategoryAlreadyPresent.ErrorMessage, HttpStatusCode.BadRequest)
        {
        }
    }

    public class CategoryNotPresentException : BaseException
    {
        public CategoryNotPresentException(int categoryId) : base(CategoryNotPresent.ErrorCode, CategoryNotPresent.ErrorMessage(categoryId), HttpStatusCode.BadRequest)
        {
        }
    }
}
