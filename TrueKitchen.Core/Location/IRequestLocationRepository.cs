﻿using System.Threading.Tasks;
using TrueKitchen.Domain.Location;

namespace TrueKitchen.Core.Location
{
    public interface IRequestLocationRepository
    {
        Task<RequestGeoLocation> GetLocationFromIPAsync(string ipAddress);
    }
}
