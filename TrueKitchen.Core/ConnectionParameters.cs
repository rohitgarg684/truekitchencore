﻿using TrueKitchen.Core.Parameters;

namespace TrueKitchen.Core
{
    public class ConnectionParameters : IAppSettings
    {
        public string ConnectionString { get; set; }
    }
}
