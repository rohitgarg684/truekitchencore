﻿using System.Threading.Tasks;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Core.File
{
    public interface IFileRepository
    {
        Task<FileSaveResponse> Save(FileUploadRequest imageUploadRequest);
        Task Delete(string fileName);
    }
}
