﻿using System.Threading.Tasks;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Core.Repository.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<Category> GetCategoryByName(string name);
    }
}
