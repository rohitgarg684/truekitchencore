﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Core.Repository.Interfaces
{
    public interface IRepository<T> :IDisposable where T : BaseEntity
    {
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter);
        Task<T> GetOne(Expression<Func<T, bool>> filter);
        Task Add(T entity);
        Task Delete(T entity);
        Task Update(T entity);
        Task Save();
        Task<bool> Any(Expression<Func<T, bool>> filter);
    }
}