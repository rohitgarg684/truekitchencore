﻿using System.Threading.Tasks;
using TrueKitchen.Core.Repository.Interfaces.User;
using TrueKitchen.Domain.File;

namespace TrueKitchen.Core.Data.Repository.User
{
    public interface IUserRepository : IBaseUserRepository<Domain.Entities.User>
    {
        Task<string> SaveProfilePhoto(FileUploadRequest fileUploadRequest, int userId);
        Task SaveProfile(Domain.Entities.User user);
    }
}
