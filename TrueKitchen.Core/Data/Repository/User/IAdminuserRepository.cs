﻿using TrueKitchen.Domain.Entities;

namespace TrueKitchen.Core.Repository.Interfaces.User
{
    public interface IAdminuserRepository : IBaseUserRepository<AdminUser>
    {
    }
}
