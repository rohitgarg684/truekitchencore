﻿using System;
using System.Threading.Tasks;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Core.Repository.Interfaces.User
{
    public interface IBaseUserRepository<T> : IRepository<T> where T: Domain.Entities.User
    {
        Task<T> GetUserByPrimaryPhoneNo(PhoneNo phoneNo);
        Task<T> CreateUser(T user);
        Task SendVerificationCode(Guid userIdentifier, PhoneNo phoneNo);
        Task VerifyPhoneNoNotificationCode(Guid userIdentifier, PhoneNo phoneNo, string verificationCode);
        Task<ValidationResponse> IsPrimaryPhoneNoAndPasswordValid(PhoneNo phoneNo, string password);
    }
}
