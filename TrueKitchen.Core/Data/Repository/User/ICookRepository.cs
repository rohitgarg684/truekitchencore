﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Core.Models.Recipe;
using TrueKitchen.Domain.Entities;
using Truekitchen.Infra.Domain.Pagination;

namespace TrueKitchen.Core.Repository.Interfaces.User
{
    public interface ICookRepository : IBaseUserRepository<Cook>
    {
        Task<Recipe> AddRecipe(AddRecipeModel addRecipeModel);
        Task<IList<Recipe>> GetRecipes(Guid cookIdentifier);
        Task<PagedResponse<Recipe>> GetRecipesByCategory(int categoryId, PageRequest pageRequest);
        Task<Recipe> GetRecipeById(int recipeId);
        Task<PagedResponse<Cook>> GetCooks(PageRequest pageRequest);
        Task UpdateRecipe(UpdateRecipeModel saveRecipeModel);
        Task DeleteRecipe(int cookId, int recipeId);
    }
}
