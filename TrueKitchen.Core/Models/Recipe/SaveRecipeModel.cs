﻿using System;
using System.Collections.Generic;
using System.Linq;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.Entities;
using TrueKitchen.Domain.Enum;
using TrueKitchen.Domain.ErrorCodes.Recipe;

namespace TrueKitchen.Core.Models.Recipe
{

    public class AddRecipeModel:RecipeModel
    {
        public  int CategoryId { get; set; }
    }

    public class UpdateRecipeModel:RecipeModel
    {
        public int RecipeId { get; set; }
    }
    public class RecipeModel
    {
        public int CookId { get; set; }
        public string RecipeName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool IsVeg { get; set; }
        public bool? IsVegan { get; set; }
        public int? ServingSize { get; set; }
        public bool? IsContainingNuts { get; set; }
        public bool? IsContainingDairy { get; set; }
        public  IList<TagModel> Tags { get; set; }
        public  IList<NutritionRecipe> NutritionRecipes { get; set; }
        public  IList<IngredientModel> Ingredients { get; set; }
        public  IList<RecipeAvailabilityModel> RecipeAvailabilities { get; set; }
    }

    public class TagModel
    {
        public string TagName { get; set; }
    }

    public static class SaveRecipeExtensionMethods
    {
        public static bool Has(this IList<TagModel> tags, string tagName)
        {
            return tags.Any(x => x.TagName == tagName);
        }

        public static bool Has(this IList<IngredientModel> ingredients, string ingredientName)
        {
            return ingredients.Any(x => x.IngredientName == ingredientName);
        }

        public static bool Has(this IList<RecipeAvailabilityModel> recipeAvailabilities, RecipeAvailability recipeAvailability)
        {
            return recipeAvailabilities.Any(x => x.GetDayOfTheWeek() == recipeAvailability.DayOfTheWeek && x.StartTime == recipeAvailability.StartTime && x.EndTime == recipeAvailability.EndTime );
        }
    }

    public class IngredientModel
    {
        public string IngredientName { get; set; }
    }

    public class RecipeAvailabilityModel
    {
        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        private string _dayOfTheWeek;
        public string DayOfTheWeek
        {
            get { return _dayOfTheWeek; }
            set
            {
                var validationResponse = new ValidationResponse();
                if (!Enum.IsDefined(typeof(DayOfTheWeek), value))
                {
                    validationResponse.AddError(new ErrorResponse(InValidDayOfTheWeek.ErrorMessage(value), InValidDayOfTheWeek.ErrorCode));
                    throw new ValidationException(validationResponse);
                }

                _dayOfTheWeek = value;
            }
        }

        public DayOfTheWeek GetDayOfTheWeek()
        {
            Enum.TryParse(_dayOfTheWeek, out DayOfTheWeek day);
            return day;
        }
    }
}
