﻿namespace TrueKitchen.Domain.Encryption
{
    public class EncryptionResponse
    {
        public string InitializationVector { get; set; }
        public string EncryptedText { get; set; }
        public byte[] Entropy { get; set; }
    }
}
