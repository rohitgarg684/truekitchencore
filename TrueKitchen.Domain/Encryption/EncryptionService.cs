﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace TrueKitchen.Domain.Encryption
{
    public class EncryptionService
    {
        private static readonly string SecretKey = "99a89914-bd03-41f0-ba5d-a4bfc169e87f";

        public static EncryptionResponse Encrypt(string clearText)
        {
            var encryptionResponse = new EncryptionResponse();

            using (var encryptor = new AesCryptoServiceProvider())
            {
                encryptionResponse.Entropy = new byte[8];
                using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
                {
                    rngCsp.GetBytes(encryptionResponse.Entropy);
                }

                Rfc2898DeriveBytes derivedBytes = new Rfc2898DeriveBytes(SecretKey, encryptionResponse.Entropy, 3);
                encryptor.Key = derivedBytes.GetBytes(32);
                encryptor.GenerateIV();
                encryptionResponse.InitializationVector = Convert.ToBase64String(encryptor.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor.CreateEncryptor(),
                        CryptoStreamMode.Write))
                    {
                        var clearBytes = Encoding.UTF8.GetBytes(clearText);
                        cryptoStream.Write(clearBytes, 0, clearBytes.Length);
                        cryptoStream.Close();
                    }

                    encryptionResponse.EncryptedText = Convert.ToBase64String(memoryStream.ToArray());
                }
            }

            return encryptionResponse;
        }
    }
}
