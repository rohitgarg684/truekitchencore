﻿using System;
using CSharpFunctionalExtensions;
using TrueKitchen.Domain.ErrorCodes.ValueObjects;

namespace TrueKitchen.Domain.ValueObjects
{
    public class Name
    {
        public Name() { }
        public string Value { get; private set; }

        private Name(string value)
        {
            Value = value;
        }

        public static Result<Name> Create(string Name)
        {
            if (string.IsNullOrWhiteSpace(Name))
                return Result.Fail<Name>("Name can’t be empty");

            if (Name.Length < 3)
                return Result.Fail<Name>("Name is having invalid length");

            return Result.Ok(new Name(Name));
        }

        public static implicit operator string(Name name)
        {
            return name.Value;
        }

        public override bool Equals(object obj)
        {
            var name = obj as Name;

            if (ReferenceEquals(name, null))
                return false;

            return Value == name.Value;
        }


        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static implicit operator Name(string name)
        {
            var nameResult = Create(name);
            if (nameResult.IsSuccess)
            {
                return nameResult.Value;
            }
            else
            {
                throw new Truekitchen.Infra.Domain.Validation.ValidationException(NameIsInvalid.ErrorMessage(nameResult.Error), NameIsInvalid.ErrorCode); ;
            }
        }
        public override string ToString()
        {
            return Value;
        }
    }
}
