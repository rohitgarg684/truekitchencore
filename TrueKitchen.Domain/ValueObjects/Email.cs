﻿using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;
using TrueKitchen.Domain.ErrorCodes.Email;

namespace TrueKitchen.Domain.ValueObjects
{
    public class Email
    {
        public Email(){}

        public string Value { get; private set; }
        private Email(string value)
        {
            Value = value;
        }

        public static Result<Email> Create(string email)
        {
            if (email!=null)
            {
                if (string.IsNullOrWhiteSpace(email))
                    return Result.Fail<Email>("E - mail can’t be empty");

                if (email.Length > 100)
                    return Result.Fail<Email>("E - mail is too long");

                if (!Regex.IsMatch(email, @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`{}|~\w])*)(?<=[0-9a-z])@))(?([)([(\d{1,3}.){3}\d{1,3}])|(([0-9a-z][-0-9a-z]*[0-9a-z]*.)+[a-z0-9][-a-z0-9]{0,22}[a-z0-9]))$"))
                    return Result.Fail<Email>("E - mail is invalid");
            }

            return Result.Ok(new Email(email));
        }

        public static implicit operator string(Email email)
        {
            return email.Value;
        }

        public override bool Equals(object obj)
        {
            var email = obj as Email;

            if (ReferenceEquals(email, null))
                return false;

            return Value == email.Value;
        }


        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static implicit operator Email(string email)
        {
            var emailResult = Create(email);
            if (emailResult.IsSuccess)
            {
                return emailResult.Value;
            }
            else
            {
                throw new Truekitchen.Infra.Domain.Validation.ValidationException(EmailIsInvalid.ErrorMessage(emailResult.Error), EmailIsInvalid.ErrorCode);
            }
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
