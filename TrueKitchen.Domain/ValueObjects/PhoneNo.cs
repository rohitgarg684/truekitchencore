﻿using System;
using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;
using TrueKitchen.Domain.ErrorCodes.ValueObjects;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.ValueObjects
{
    public class PhoneNo
    {
        public PhoneNo() { }

        public string Value { get; private set; }
        private PhoneNo(string value)
        {
            Value = value;
        }

        public static Result<PhoneNo> Create(string phoneNo)
        {
            if (string.IsNullOrWhiteSpace(phoneNo))
                return Result.Fail<PhoneNo>("Phone No can’t be empty");

            bool isIndiaPhoneNo = phoneNo.StartsWith("+91");
            bool isUSAPhoneNo = phoneNo.StartsWith("+1");

            if ((isIndiaPhoneNo && phoneNo.Length != 13) || (isUSAPhoneNo && phoneNo.Length != 12))
            {
                return Result.Fail<PhoneNo>("Its length should be 10 after country code");
            }

            if(!isIndiaPhoneNo && !isUSAPhoneNo)
            {
                return Result.Fail<PhoneNo>("Phone No should be either of US or India and should start with + 91 or + 1");
                
            }

            if (!Regex.IsMatch(phoneNo.Substring(3), @"^[0-9]*$"))
                return Result.Fail<PhoneNo>("Phone No is invalid");

            return Result.Ok(new PhoneNo(phoneNo));
        }

        public static implicit operator string(PhoneNo phoneNo)
        {
            return phoneNo.Value;
        }

        public override bool Equals(object obj)
        {
            var phoneNo = obj as PhoneNo;

            if (ReferenceEquals(phoneNo, null))
                return false;

            return Value == phoneNo.Value;
        }


        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static implicit operator PhoneNo(string phoneNo)
        {
            var phoneNoResult = Create(phoneNo);
            if (phoneNoResult.IsSuccess)
            {
                return phoneNoResult.Value;
            }
            else
            {
                throw new ValidationException(PhoneNoIsInvalid.ErrorMessage(phoneNoResult.Error), PhoneNoIsInvalid.ErrorCode); ;
            }
        }

        public override string ToString()
        {
            return Value;
        }

    }
}
