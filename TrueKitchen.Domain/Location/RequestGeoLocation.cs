﻿namespace TrueKitchen.Domain.Location
{
    public class RequestGeoLocation
    {
        public string CountryName { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
