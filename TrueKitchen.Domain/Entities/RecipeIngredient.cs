﻿namespace TrueKitchen.Domain.Entities
{
    public class RecipeIngredient : BaseEntity
    {
        public RecipeIngredient() { }
        public RecipeIngredient(string name,int recipeId)
        {
            RecipeId = recipeId;
            Name = name;
        }

        public virtual int RecipeIngredientId { get; set; }
        public virtual string Name { get; set; }
        public virtual int RecipeId { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
