﻿namespace TrueKitchen.Domain.Entities
{
    public class KitchenAvailability:Availability
    {
        public virtual int CookId { get; set; }
        public virtual Cook Cook { get; set; }

        public bool Equals(KitchenAvailability other)
        {
            return CookId == other.CookId && base.Equals(other);
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as KitchenAvailability);
        }
    }
}
