﻿using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Domain.Entities
{
    public class UserProfile
    {
        public virtual int UserId { get; set; }
        public virtual PhoneNo PhoneNo { get; set; }
        public virtual int AddressId { get; set; }
        public virtual Address Address { get; set; }
        public virtual int PasswordId { get; set; }
        public virtual UserPassword UserPassword { get; set; }
    }
}
