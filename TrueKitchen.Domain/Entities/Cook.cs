﻿using System.Collections.Generic;
using System.Linq;
using TrueKitchen.Domain.Enum;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.Entities
{
    public class Cook: User
    {
        public Cook()
        {
            UserType = UserType.Cook;
            Recipes = new List<Recipe>();
            KitchenAvailabilities = new List<KitchenAvailability>();
        }

        public virtual string KitchenName { get; set; }
        public virtual string KitchenDescription { get; set; }
        public virtual UserAadharCard UserAadharCard { get; set; }
        public virtual int UserAadharCardId { get; set; }
        public virtual ICollection<Recipe> Recipes { get; set; }
        public virtual bool IsKitchenAvailable { get; set; }
        public virtual ICollection<KitchenAvailability> KitchenAvailabilities { get; set; }

        public IEnumerable<RecipeTag> GetTagsByRecipeId(int recipeId)
        {
            var recipe = Recipes.Where(x => x.RecipeId == recipeId).FirstOrDefault();
            if(recipe == null)
            {
                new ValidationException($"RecipeId is invalid for cook - {Identifier}", 4343);
            }

            return recipe.RecipeTags;
        }
    }
}
