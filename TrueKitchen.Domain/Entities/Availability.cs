﻿using System;
using TrueKitchen.Domain.Enum;

namespace TrueKitchen.Domain.Entities
{
    public abstract class Availability
    {
        public virtual int RecipeAvailabilityId { get; set; }
        public virtual TimeSpan StartTime { get; set; }
        public virtual TimeSpan EndTime { get; set; }
        public virtual DayOfTheWeek DayOfTheWeek { get; set; }

        public bool Equals(Availability other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return other.StartTime == StartTime
                && other.EndTime == EndTime
                && other.DayOfTheWeek == DayOfTheWeek;
        }
    }
}
