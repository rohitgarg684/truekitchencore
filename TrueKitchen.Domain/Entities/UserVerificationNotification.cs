﻿using System;
using System.Threading.Tasks;
using TrueKitchen.Domain.ErrorCodes.User;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Domain.Entities
{
    public class UserVerificationNotification:BaseEntity
    {
        public UserVerificationNotification()
        {
            CreatedDate = DateTime.UtcNow;
        }

        public virtual int UserVerificationNotificationId { get; set; }
        public virtual User User { get; set; }
        public virtual int UserId { get; set; }
        public virtual string VerificationMode  { get; set; }
        public virtual string VerificationEntityValue { get; set; }
        public virtual string VerificationFor { get; set; }
        public virtual string VerificationCode { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiryDate { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsVerified { get; set; }

        public static string GenerateCode()
        {
            var random = new Random();
            return random.Next(0, 9999).ToString("D4");
        }

        public static async Task<ValidationResponse> ValidatePhoneNotification(User user, PhoneNo phoneNo)
        {
            var validationResponse = new ValidationResponse();
            if ((user.PrimaryPhoneNo.Value == phoneNo.Value && user.IsPrimaryPhoneNoVerified) || (user.SecondryPhoneNo == phoneNo.Value && user.IsSecondryPhoneNoVerified))
            {
                validationResponse.AddError(UserPhoneNoAlreadyActive.ErrorCode, UserPhoneNoAlreadyActive.ErrorMessage(phoneNo.Value));
                return validationResponse;
            }

            if (user.PrimaryPhoneNo.Value != phoneNo.Value && user.SecondryPhoneNo != phoneNo.Value)
            {
                validationResponse.AddError(PhoneNoDoesNotBelongToUser.ErrorCode, PhoneNoDoesNotBelongToUser.ErrorMessage(phoneNo.Value));
                return validationResponse;
            }

            return validationResponse;
        }
    }
}
