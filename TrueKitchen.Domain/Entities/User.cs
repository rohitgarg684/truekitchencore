﻿using System;
using System.Threading.Tasks;
using TrueKitchen.Domain.Enum;
using TrueKitchen.Domain.ErrorCodes.User;
using TrueKitchen.Domain.Resolver;
using TrueKitchen.Domain.Services.User.BaseUser;
using Truekitchen.Infra.Domain.Validation;
using TrueKitchen.Domain.ValueObjects;

namespace TrueKitchen.Domain.Entities
{
    public abstract class User:BaseEntity
    {
        protected User()
        {
            CreateDateTime = DateTime.UtcNow;
            Identifier = Guid.NewGuid();
        }

        public virtual int UserId { get; set; }
        public virtual Guid Identifier { get; set; }
        public virtual Name FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual Name LastName { get; set; }
        public virtual Email Email { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual DateTime CreateDateTime { get; set; }
        public virtual UserPassword UserPassword { get; set; }
        public virtual int UserPasswordId { get; set; }
        public virtual UserAttachment UserAttachment { get; set; }
        public virtual int? ProfilePhotoId { get; set; }
        public virtual PhoneNo PrimaryPhoneNo { get; set; }
        public virtual string SecondryPhoneNo { get; set; }
        public virtual Address Address { get; set; }
        public virtual int? AddressId { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsPrimaryPhoneNoVerified { get; set; }
        public virtual bool IsSecondryPhoneNoVerified { get; set; }

        public async Task<ValidationResponse> ValidateUser(Func<string, IResolver> resolverFactory)
        {
            var validationResponse = new ValidationResponse();
            var userValidationService = (IUserValidationService)resolverFactory(typeof(IUserValidationService).ToString());

            //email validation
            var emailValidationResponse = await userValidationService.ValidateEmail(this);
            validationResponse.AddErrors(emailValidationResponse.ErrorResponses);

            //primary phoneno validation
            var primaryPhoneNoValidationResponse = await userValidationService.ValidatePrimaryPhoneNo(this);
            validationResponse.AddErrors(primaryPhoneNoValidationResponse.ErrorResponses);

            //validatePassword
            var userPasswordValidationResponse = UserPassword.ValidatePassword(this.UserPassword.Value);
            validationResponse.AddErrors(userPasswordValidationResponse.ErrorResponses);

            if(SecondryPhoneNo != null)
            {
                var phoneResult = PhoneNo.Create(SecondryPhoneNo);
                if (phoneResult.IsFailure)
                {
                    validationResponse.AddError(PhoneNoIsInvalid.ErrorCode, phoneResult.Error);
                }
            }

            if (Address != null)
            {
                var addressValidationResponse = await Address.Validate(resolverFactory);
                validationResponse.AddErrors(addressValidationResponse.ErrorResponses);
            }

            return validationResponse;
        }

        public ValidationResponse ValidatePhoneNoAlreadyVerified(PhoneNo phoneNo)
        {
            var validationResponse = new ValidationResponse();

            if ((PrimaryPhoneNo.Value == phoneNo.Value && IsPrimaryPhoneNoVerified)
              || (SecondryPhoneNo == phoneNo.Value && IsSecondryPhoneNoVerified))
            {
                validationResponse.AddError(UserAlreadyVerifiedWithPhoneNo.ErrorCode, UserAlreadyVerifiedWithPhoneNo.ErrorMessage(phoneNo.Value));
            }

            return validationResponse;
        }
    }
}
