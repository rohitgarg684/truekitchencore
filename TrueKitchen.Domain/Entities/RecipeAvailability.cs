﻿using System;

namespace TrueKitchen.Domain.Entities
{
    public class RecipeAvailability : Availability, IEquatable<RecipeAvailability>
    { 
        public virtual int RecipeId { get; set; }
        public virtual Recipe Recipe { get; set; }

        public bool Equals(RecipeAvailability other)
        {
            return RecipeId == other.RecipeId && base.Equals(other);
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as RecipeAvailability);
        }
    }
}
