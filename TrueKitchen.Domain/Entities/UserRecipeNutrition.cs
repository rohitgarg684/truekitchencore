﻿namespace TrueKitchen.Domain.Entities
{
    public class UserRecipeNutrition
    {
        public virtual int UserId { get; set; }
        public virtual Cook Cook { get; }
    }
}
