﻿using System;
using TrueKitchen.Domain.Encryption;

namespace TrueKitchen.Domain.Entities
{
    public abstract class SensitiveInfo
    {
        protected SensitiveInfo() { }
        protected SensitiveInfo(string clearText)
        {
            Value = clearText;
            var encryptionResponse = EncryptionService.Encrypt(clearText);
            Hash = encryptionResponse.InitializationVector;
            Entropy = encryptionResponse.Entropy;
            CreateDateTime=DateTime.UtcNow;
            EncryptedValue = encryptionResponse.EncryptedText;
        }
        public virtual int SensitiveInfoId { get; private set; }
        public virtual string Hash { get; private set; }
        public virtual byte[] Entropy { get; private set; }
        public virtual string EncryptedValue { get; private set; }
        public virtual DateTime CreateDateTime { get; private set; }
        public virtual string Value { get; private set; }
    }
}
