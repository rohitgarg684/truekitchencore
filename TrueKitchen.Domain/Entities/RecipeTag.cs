﻿namespace TrueKitchen.Domain.Entities
{
    public class RecipeTag : BaseEntity
    {
        public RecipeTag()
        {
        }

        public RecipeTag(string tagName, int recipeId)
        {
            RecipeId = recipeId;
            TagName = tagName;
        }

        public virtual int RecipeTagId { get; set; }
        public virtual string TagName { get; set; }
        public virtual int RecipeId { get; set; }

        public virtual Recipe Recipe { get; set; }

        public override bool Equals(object obj)
        {
            var recipeTag = obj as RecipeTag;
            return recipeTag.TagName == TagName && recipeTag.RecipeId == RecipeId;
        }
    }
}
