﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrueKitchen.Domain.ErrorCodes;
using TrueKitchen.Domain.ErrorCodes.Recipe;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.Entities
{
    public class Recipe: BaseEntity
    {
        public Recipe()
        {
            RecipeTags = new List<RecipeTag>();
            RecipeIngredients = new List<RecipeIngredient>();
            PreparationTimeInMin = 60; //default prep time
            IsRecipeAvailable = true; //default recipe is available
            RecipeAvailabilities = new List<RecipeAvailability>();
            //NutritionRecipes = new List<NutritionRecipe>();
        }

        public virtual int RecipeId { get; set; }
        public virtual string RecipeName { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Price { get; set; }
        public virtual bool IsVeg { get; set; }
        public virtual bool? IsVegan { get; set; }
        public virtual int? ServingSize { get; set; }
        public virtual bool? IsContainingNuts { get; set; }
        public virtual bool? IsContainingDairy { get; set; }

        public virtual ICollection<RecipeTag> RecipeTags { get; set; }
        public virtual int CookId { get; set; }
        public virtual Cook Cook { get; set; }
        //public virtual ICollection<NutritionRecipe> NutritionRecipes { get; set; }
        public virtual ICollection<RecipeIngredient> RecipeIngredients { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual bool IsRecipeAvailable { get; set; }
        public virtual int PreparationTimeInMin { get; set; }
        public virtual ICollection<RecipeAvailability> RecipeAvailabilities { get; set; }

        public bool  IsTagPresent(RecipeTag recipeTag)
        {
            return RecipeTags.Any(x => x.Equals(recipeTag));
        }

        public void AddRecipeTags(IList<string> tags)
        {
            tags.Distinct().ToList().ForEach(x => RecipeTags.Add(new RecipeTag(x, RecipeId)));
        }

        public void AddRecipeIngredients(IList<string> ingredients)
        {
            ingredients.Distinct().ToList().ForEach(x => RecipeIngredients.Add(new RecipeIngredient(x, RecipeId)));
        }

        public static async Task<ValidationResponse> ValidateExistingRecipeId(Cook cook, int recipeId)
        {
            var validationResponse =  new ValidationResponse();
            var recipe = cook.Recipes.FirstOrDefault(x => x.RecipeId == recipeId);
            if (recipe == null)
            {
                validationResponse.AddError(RecipeNotFound.ErrorCode, RecipeNotFound.ErrorMessage);
            }

            return validationResponse;
        }

        public static async Task<ValidationResponse> ValidateRecipeName(string recipeName)
        {
            var validationResponse = new ValidationResponse();
            var allowedRecipeLength = 200;

            if (string.IsNullOrEmpty(recipeName))
                validationResponse.AddError(RecipeNameInvalid.ErrorCode, RecipeNameInvalid.ErrorMessage("Recipe name can notbe empty"));
           
            else if (recipeName.Length > allowedRecipeLength)
                validationResponse.AddError(RecipeNameInvalid.ErrorCode, RecipeNameInvalid.ErrorMessage($"Recipe name can not greater than {allowedRecipeLength}"));

            return validationResponse;
        }

        public static async Task<ValidationResponse> ValidateRecipeAlreadyPresentForCook(Cook cook, int recipeId, string recipeName)
        {
            var validationResponse = new ValidationResponse();
            var isRecipeAlreadyPresent = cook.Recipes.Any(x => x.RecipeName == recipeName && x.RecipeId != recipeId);
            if (isRecipeAlreadyPresent)
                validationResponse.AddError(RecipeAlreadyPresent.ErrorCode, RecipeAlreadyPresent.ErrorMessage(recipeName));

            return validationResponse;
        }

    }
}
