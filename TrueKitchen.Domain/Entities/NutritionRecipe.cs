﻿using TrueKitchen.Domain.Enum;

namespace TrueKitchen.Domain.Entities
{
    public class NutritionRecipe
    {
        public virtual int NutritionRecipeId { get; set; }
        public virtual int NutritionId { get; set; }
        public virtual Nutrition Nutrition { get; set; }
        public virtual int RecipeId { get; set; }
        public virtual Recipe Recipe { get; set; }
        public virtual decimal NutritionValue { get; set; }
        public virtual NutritionMeasingUnit NutritionMeasingUnit { get; set; }
    }
}
