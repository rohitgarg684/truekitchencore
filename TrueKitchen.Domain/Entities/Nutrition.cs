﻿using TrueKitchen.Domain.Enum;

namespace TrueKitchen.Domain.Entities
{
    public class Nutrition
    {
        public virtual int NutritionId { get; set; }
        public virtual string NutritionName { get; set; }
        public virtual NutritionMeasingUnit NutritionMeasingUnit { get; set; }
    }
}
