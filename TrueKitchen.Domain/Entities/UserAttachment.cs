﻿using System;

namespace TrueKitchen.Domain.Entities
{
    public class UserAttachment
    {
        public UserAttachment()
        {
            CreatedDate = DateTime.UtcNow;
        }

        public virtual int AttachmentId { get; set; }
        public virtual string AttachmentName { get; set; }
        public virtual string CanonicalName { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual int UserId { get; set; }
    }
}
