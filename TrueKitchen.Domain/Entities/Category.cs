﻿namespace TrueKitchen.Domain.Entities
{
    public class Category: BaseEntity
    {
        public virtual int CategoryId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}
