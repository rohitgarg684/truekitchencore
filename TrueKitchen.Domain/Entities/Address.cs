﻿using System;
using System.Threading.Tasks;
using TrueKitchen.Domain.ErrorCodes.User;
using TrueKitchen.Domain.Resolver;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.Entities
{
    public class Address
    {
        public virtual int AddressId { get; set; }
        public virtual string AddressLine1 { get; set; }
        public virtual string AddressLine2 { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }

        public async Task<ValidationResponse> Validate(Func<string, IResolver> resolverFactory)
        {
            var validationResponse = new ValidationResponse();
            if (string.IsNullOrEmpty(AddressLine1))
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, "Address line 1 is required");
            }

            int addressLineMaxLength = 200;
            int addressLineMinLength = 4;
            if (AddressLine1.Length > addressLineMaxLength || AddressLine1.Length < addressLineMinLength)
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, $"Address line 1 length must be between {addressLineMinLength} and {addressLineMaxLength} ");
            }

            if(AddressLine2!=null && (AddressLine2.Length > addressLineMaxLength || AddressLine2.Length < addressLineMinLength))
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, $"Either Address line 2 should be empty or its length must be between {addressLineMinLength} and {addressLineMaxLength} ");
            }

            if (string.IsNullOrEmpty(Country))
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, "Country is required");
            }

            if (Country!="India" &&  Country !="USA")
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, "Currently we only support our applicaiton For country India and USA");
            }

            int zipCodeMaxLength = 10;
            int zipCodeMinLength = 4;

            if (string.IsNullOrEmpty(ZipCode) || (ZipCode.Length> zipCodeMaxLength || ZipCode.Length< zipCodeMinLength))
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, $"Zip Code is required and its length must be between {zipCodeMinLength} and {zipCodeMaxLength}");
            }

            if (string.IsNullOrEmpty(State))
            {
                validationResponse.AddError(AddressIsInvalid.ErrorCode, "State is required");
            }

            return validationResponse;
        }
    }
}
