﻿using TrueKitchen.Domain.ErrorCodes.User;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.Entities
{
    public class UserPassword : SensitiveInfo
    {
        public UserPassword()
        {
        }

        public UserPassword(string password):base(password)
        {
            NoOfRetires = 0;
            isUserLocked = false;
        }

        public virtual int NoOfRetires { get; set; }
        public virtual bool isUserLocked { get; set; }

        public static ValidationResponse ValidatePassword(string password)
        {
            var validationResponse = new ValidationResponse();

            const int MIN_LENGTH = 8;
            const int MAX_LENGTH = 20;

            bool meetsLengthRequirements = password != null && password.Length >= MIN_LENGTH && password.Length <= MAX_LENGTH;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in password)
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            bool isValid = meetsLengthRequirements
                        && hasUpperCaseLetter
                        && hasLowerCaseLetter
                        && hasDecimalDigit;

            if (!isValid)
                validationResponse.AddError(UserPasswordNotValid.ErrorCode, UserPasswordNotValid.ErrorMessage(MIN_LENGTH,MAX_LENGTH));

            return validationResponse;
        }

       
    }
}
