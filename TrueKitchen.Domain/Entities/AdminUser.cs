﻿using TrueKitchen.Domain.Enum;

namespace TrueKitchen.Domain.Entities
{
    public class AdminUser:User
    {
        public AdminUser()
        {
            UserType = UserType.Admin;
        }
        public virtual string UserCreationReason { get; set; }
    }
}
