﻿namespace TrueKitchen.Domain.Enum
{
    public enum NutritionMeasingUnit
    {
        g,
        mcg,
        Per,
        mg
    }
}
