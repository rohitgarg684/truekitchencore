﻿namespace TrueKitchen.Domain.Enum
{
    public static class UserVerificationMode
    {
        public const string Email = "Email";
        public const string Mobile = "Mobile";
    }
}
