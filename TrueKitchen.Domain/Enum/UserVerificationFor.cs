﻿namespace TrueKitchen.Domain.Enum
{
    public static class UserVerificationFor
    {
        public const string ForgotPassword = "ForgotPassword";
        public const string CreateUser = "CreateUser";
    }
}
