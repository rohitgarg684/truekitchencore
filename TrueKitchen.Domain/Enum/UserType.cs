﻿namespace TrueKitchen.Domain.Enum
{
    public enum UserType
    {
        Admin,
        Customer,
        Cook
    }
}
