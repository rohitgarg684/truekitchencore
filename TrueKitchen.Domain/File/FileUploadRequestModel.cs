﻿using System.IO;

namespace TrueKitchen.Domain.File
{
    public class FileUploadRequest
    {
        public string ContentType { get; set; }
        public int FileSize{get; set;}
        public string FileName { get; set; }
        public Stream Stream { get; set; }
    }
}
