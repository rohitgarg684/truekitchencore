﻿namespace TrueKitchen.Domain.File
{
    public class FileSaveResponse
    {
        public FileSaveResponse(string canonicalName, string url)
        {
            CanonicalName = canonicalName;
            Url = url;
        }
        public string CanonicalName { get; set; }
        public string Url { get; set; }
    }
}
