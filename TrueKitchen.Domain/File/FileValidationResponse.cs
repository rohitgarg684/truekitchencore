﻿namespace TrueKitchen.Domain.File
{
    public class FileValidationResponse
    {
        public FileValidationResponse() { }

        public FileValidationResponse(string message, int code)
        {
            Message = message;
            Code = code;
        }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
