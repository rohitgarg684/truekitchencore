﻿namespace TrueKitchen.Domain.ErrorCodes
{
    public class CategoryNotPresent
    {
        public static int ErrorCode => 130;
        public static string ErrorMessage(int categoryId) => $"Category - {categoryId} is not present in system";
    }
}
