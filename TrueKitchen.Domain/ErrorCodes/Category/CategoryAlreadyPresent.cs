﻿namespace TrueKitchen.Domain.ErrorCodes
{
    public class CategoryAlreadyPresent
    {
        public static int ErrorCode => 132;
        public static string ErrorMessage => "Category already present";
    }
}
