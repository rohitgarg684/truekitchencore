﻿namespace TrueKitchen.Domain.ErrorCodes
{
    public class RecipeNotFound
    {
        public static int ErrorCode => 340;
        public static string ErrorMessage => "Recipe Not found";
    }
}
