﻿namespace TrueKitchen.Domain.ErrorCodes.Recipe
{
    public class RecipeAlreadyPresent
    {
        public static int ErrorCode => 342;
        public static string ErrorMessage(string recipeName) => $"Recipe - {recipeName} already present for the cook. Please use a different recipe name";
    }
}
