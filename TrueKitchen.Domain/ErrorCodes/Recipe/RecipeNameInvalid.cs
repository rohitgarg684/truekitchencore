﻿namespace TrueKitchen.Domain.ErrorCodes.Recipe
{
    public class RecipeNameInvalid
    {
        public static int ErrorCode => 341;
        public static string ErrorMessage (string msg)=> msg;
    }
}
