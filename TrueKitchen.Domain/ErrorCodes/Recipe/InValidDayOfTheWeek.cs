﻿namespace TrueKitchen.Domain.ErrorCodes.Recipe
{
    public class InValidDayOfTheWeek
    {
        public static int ErrorCode => 399;
        public static string ErrorMessage(string dayOfTheWeek) => $"Day of the week - {dayOfTheWeek} is invalid";
    }
}
