﻿namespace TrueKitchen.Domain.ErrorCodes.ValueObjects
{
    public class NameIsInvalid
    {
        public static int ErrorCode => 841;
        public static string ErrorMessage(string msg) => msg;
    }
}
