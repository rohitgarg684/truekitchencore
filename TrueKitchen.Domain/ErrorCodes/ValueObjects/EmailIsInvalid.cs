﻿namespace TrueKitchen.Domain.ErrorCodes.Email
{
   public class EmailIsInvalid
    {
        public static int ErrorCode => 840;
        public static string ErrorMessage (string msg) => msg;
    }
}
