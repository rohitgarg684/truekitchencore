﻿namespace TrueKitchen.Domain.ErrorCodes.ValueObjects
{
    public class PhoneNoIsInvalid
    {
        public static int ErrorCode => 842;
        public static string ErrorMessage(string msg) => msg;
    }
}
