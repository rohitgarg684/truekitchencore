﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserPhoneNoVerificationIntervalError
    {
        public static int ErrorCode => 185;
        public static string ErrorMessage(int interval) => $"Maxmum tries have been reached for this phone number. Please wait {interval} hrs for next try.";
    }
}
