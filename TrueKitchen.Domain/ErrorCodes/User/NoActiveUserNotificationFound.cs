﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserNotificationCodeExpired
    {
        public static int ErrorCode => 981;
        public static string ErrorMessage(string phoneNo) => $"The OTP Code expired. Please generate a new one and verify your phone number";
    }
}
