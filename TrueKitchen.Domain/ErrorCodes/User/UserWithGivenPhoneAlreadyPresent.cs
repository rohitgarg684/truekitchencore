﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserWithGivenPhoneAlreadyPresent
    {
        public static int ErrorCode => 182;
        public static string ErrorMessage (string phoneNo) => $"This phone number is already associated with a different User. Please login with primary phone no {phoneNo}";
    }
}
