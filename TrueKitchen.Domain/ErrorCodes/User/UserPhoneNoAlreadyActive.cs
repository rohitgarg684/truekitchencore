﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserPhoneNoAlreadyActive
    {
        public static int ErrorCode => 187;
        public static string ErrorMessage(string phoneNo) => $"User phone no  - {phoneNo}  is already active";

    }
}
