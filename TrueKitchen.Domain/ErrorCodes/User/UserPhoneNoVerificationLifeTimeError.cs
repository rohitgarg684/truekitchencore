﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserPhoneNoVerificationLifeTimeError
    {
        public static int ErrorCode => 184;
        public static string ErrorMessage(int interval) => $"Maximum lifetime tries - {interval} occured for this phone number. No further verification messages can be send";
    }
}
