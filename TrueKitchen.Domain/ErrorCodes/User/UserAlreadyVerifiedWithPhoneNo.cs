﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserAlreadyVerifiedWithPhoneNo
    {
        public static int ErrorCode => 983;
        public static string ErrorMessage(string phoneNo) => $"Phone No - {phoneNo} is already verified.";
    }
}
