﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public static class UserWithGivenEmailAlreadyPresent
    {
        public static int ErrorCode => 180;
        public static string ErrorMessage => $"User already present with this email.";
    }
}
