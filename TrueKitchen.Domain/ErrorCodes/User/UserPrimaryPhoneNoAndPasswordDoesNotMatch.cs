﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserPrimaryPhoneNoAndPasswordDoesNotMatch
    {
        public static int ErrorCode => 386;
        public static string ErrorMessage => $"Primary PhoneNo and password does not match. Please try again.";
    }
}
