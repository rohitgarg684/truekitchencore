﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserNameAndPasswordMismatch
    {
        public static int ErrorCode => 567;
        public static string ErrorMessage => $"userName And password mismatch";
    }
}
