﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserVerificationCodeMismatch
    {
        public static int ErrorCode => 982;
        public static string ErrorMessage(string phoneNo, string verificationCode) => $"The verification code - {verificationCode} does not match against phoneNo - {phoneNo}";
    }
}
