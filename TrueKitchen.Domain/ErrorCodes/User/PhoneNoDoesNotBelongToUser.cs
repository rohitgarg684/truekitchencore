﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class PhoneNoDoesNotBelongToUser
    {
        public static int ErrorCode => 188;
        public static string ErrorMessage(string phoneNo) => $"Phone no  - {phoneNo}  does not belong to this user";
    }
}
