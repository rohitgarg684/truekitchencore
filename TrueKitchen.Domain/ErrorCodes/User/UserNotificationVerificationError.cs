﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class NoActiveUserNotificationFound
    {
        public static int ErrorCode => 980;
        public static string ErrorMessage(string phoneNo) => $"No active OTP found in the system for this phone no - {phoneNo}";
    }
}
