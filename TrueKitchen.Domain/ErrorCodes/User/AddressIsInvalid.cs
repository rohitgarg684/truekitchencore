﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class AddressIsInvalid
    {
        public static int ErrorCode => 189;
        public static string ErrorMessage(string msg) => msg;
    }
}
