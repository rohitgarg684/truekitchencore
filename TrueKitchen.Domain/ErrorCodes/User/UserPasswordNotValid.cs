﻿namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserPasswordNotValid
    {
        public static int ErrorCode => 184;
        public static string ErrorMessage(int minLength, int maxLength) => $"User password is invalid. Password must contains one uppercase, one lowercase, one digit and must be atleast {minLength} in legth. Maximum password length is {maxLength}";

    }
}
