﻿using System;

namespace TrueKitchen.Domain.ErrorCodes.User
{
    public class UserNotFoundWithProvidedIdentifier
    {
        public static int ErrorCode => 186;
        public static string ErrorMessage(Guid identifier) => $"No user found in the system with id : {identifier}";

    }
}
