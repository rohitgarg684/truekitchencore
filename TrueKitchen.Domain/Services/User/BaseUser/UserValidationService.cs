﻿using System.Threading.Tasks;
using TrueKitchen.Domain.Resolver;
using Truekitchen.Infra.Domain.Validation;

namespace TrueKitchen.Domain.Services.User.BaseUser
{
    public interface IUserValidationService:IResolver
    {
        Task<ValidationResponse> ValidateEmail(Entities.User user);
        Task<ValidationResponse> ValidatePrimaryPhoneNo(Entities.User user);
    }
}
