﻿namespace TrueKitchen.Domain.Security
{
    public class GenerateTokenResponse
    {
        public GenerateTokenResponse(string token, int ttl)
        {
            Token = token;
            TTL = ttl;
        }

        public string Token { get; set; }
        public int TTL { get; set; }
    }
}
