﻿namespace TrueKitchen.Domain.Security
{
    public class TokenValidationResponse
    {
        public string UserName { get; set; }
        public bool IsUserValidated { get; set; }
    }
}
